package gov.dss.sdk.http

import com.google.inject.Inject
import gov.dss.sdk.service.module.TestGuiceModule
import groovy.json.JsonSlurper

import static com.google.inject.Guice.createInjector

/**
 * Test to exercise the JsonResponseHandler
 *
 * Author : Carvel 
 * Date   : 5/9/16
 */
class JsonResponseHandlerTest extends GroovyTestCase {

    @Inject
    ResponseHandler<String, Map<String, Object>> handler

    void setUp() {
        createInjector(new TestGuiceModule(this));
    }

    void testDependencyInjection() {
        //--- make assertions
        assert handler != null
        assert ((JsonResponseHandler) handler).errorMessages != null
    }


    void test200() {
        //--- create test data
        def expectedResult = '{"status":200,"payload":{"messages":["This is a message","This is another message"]}}'
        def messageList = ["This is a message", "This is another message"]
        def messageMap = [messages: messageList]
        def responseMap = [status: 200, payload: messageMap]

        //--- exercise method
        def result = handler.generate(responseMap)

        //--- make assertions
        assert result != null
        assert result == expectedResult
    }

    void test400() {
        exerciseErrorCode(
                400
                , "Bad Request"
                , "The server cannot or will not process the request due to something that is perceived to be a client error"
                , "https://httpstatuses.com/400"
        )
    }

    void test401() {
        exerciseErrorCode(
                401
                , "Unauthorized"
                , "The request has not been applied because it lacks valid authentication credentials for the target resource."
                , "https://httpstatuses.com/401"
        )
    }

    void test403() {
        exerciseErrorCode(
                403
                , "Forbidden"
                , "The server understood the request but refuses to authorize it."
                , "https://httpstatuses.com/403"
        )
    }

    void test404() {
        exerciseErrorCode(
                404
                , "Not Found"
                , "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists."
                , "https://httpstatuses.com/404"
        )
    }

    void test405() {
        exerciseErrorCode(
                405
                , "Method Not Allowed"
                , "The method received in the request-line is known by the origin server but not supported by the target resource."
                , "https://httpstatuses.com/405"
        )
    }

    void test500() {
        exerciseErrorCode(
                500
                , "Internal Server Error"
                , "The server encountered an unexpected condition that prevented it from fulfilling the request."
                , "https://httpstatuses.com/500"
        )
    }

    void exerciseErrorCode(status, developerMessage, userMessage, moreInformation) {
        //--- create test data
        def response = [status: status]
        //--- exercise method
        def result = handler.generate(response)
        //--- make assertions
        assert result != null
        def slurper = new JsonSlurper()
        def parsedResult = slurper.parseText(result)
        assert parsedResult['status'] == status
        assert parsedResult['developerMessage'] == developerMessage
        assert parsedResult['userMessage'] == userMessage
        assert parsedResult['moreInformation'] == moreInformation
    }

}
