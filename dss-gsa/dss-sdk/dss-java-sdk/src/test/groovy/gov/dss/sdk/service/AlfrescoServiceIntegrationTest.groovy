package gov.dss.sdk.service

import com.google.inject.Guice
import gov.dss.sdk.service.module.TestGuiceModule
import org.apache.chemistry.opencmis.client.api.Document
import org.junit.Ignore

import javax.inject.Inject

import static gov.dss.sdk.service.MessageService.*

/**
 * Integration test for the AlfrescoService
 *
 * Created by Carvel on 4/25/16 @ 9:18 AM.
 */
@Ignore
public class AlfrescoServiceIntegrationTest extends GroovyTestCase {

    @Inject
    AlfrescoService alfrescoService

    String filename = "DssSample.pdf"

    void setUp() {
        Guice.createInjector(new TestGuiceModule(this));
        alfrescoService.sendMessage([(ACTION): (DELETE), (FILE_NAME): filename])
    }

    void testDependencyInjection() {
        //--- make assertions
        assert alfrescoService != null
        assert ((AlfrescoService) alfrescoService).getProperty("username") != null
        assert ((AlfrescoService) alfrescoService).getProperty("password") != null
        assert ((AlfrescoService) alfrescoService).getProperty("path") != null
        assert ((AlfrescoService) alfrescoService).getProperty("atomPubUrl") != null
    }


    public void testSendMessage() throws Exception {
        //--- setup test data
        MessageServiceResponse response = new LocalFileService().sendMessage([(ACTION): (FIND), (FILE_NAME): filename])
        File pdfFile = response.payload as File

        //--- exercise method
        MessageServiceResponse result = alfrescoService.sendMessage([(ACTION): (UPLOAD), (FILE): pdfFile])

        //--- make assertions
        assert result != null
        assert (result.payload as Document).name == filename

    }

}