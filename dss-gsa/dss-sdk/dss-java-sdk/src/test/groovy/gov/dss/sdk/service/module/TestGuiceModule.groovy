package gov.dss.sdk.service.module

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.name.Names
import gov.dss.sdk.annotations.*
import gov.dss.sdk.http.JsonResponseHandler
import gov.dss.sdk.http.ResponseHandler
import gov.dss.sdk.service.*
import net.jmob.guice.conf.core.ConfigurationModule
import org.springframework.core.io.ClassPathResource

import javax.inject.Named

/**
 * Author : Carvel 
 * Date   : 4/13/16
 */
class TestGuiceModule extends AbstractModule {
    private final Object instance

    public TestGuiceModule(Object instance) {
        this.instance = instance
    }

    @Override
    protected void configure() {
        install(ConfigurationModule.create());
        requestInjection(instance);
        try {
            Properties props = new Properties()
            props.load(new FileInputStream(new ClassPathResource("application.properties").getFile(

            ).getAbsolutePath()))
            Names.bindProperties(binder(), props)
        } catch (Exception e) {
            e.printStackTrace()
        }
        bind(MessageService.class).annotatedWith(GmailServiceAnnotation.class).to(GmailService.class);
        bind(MessageService.class).annotatedWith(GoogleDriveServiceAnnotation.class).to(GoogleDriveService2LO.class);
        bind(MessageService.class).annotatedWith(LocalFileServiceAnnotation.class).to(LocalFileService.class);
        bind(MessageService.class).annotatedWith(AlfrescoServiceAnnotation.class).to(AlfrescoService.class);
        bind(MessageService.class).annotatedWith(TspIntegrationServiceAnnotation.class).to(TspIntegrationService.class);
    }

    @Provides
    ResponseHandler<String, Map<String, Object>> providesJsonResponseHandler(
            @Named("errorMessages") String errorMessages) {
        ResponseHandler<String, Map<String, Object>> handler = new JsonResponseHandler<String, Map<String, Object>>()
        handler.errorMessages = errorMessages
        handler
    }

}

