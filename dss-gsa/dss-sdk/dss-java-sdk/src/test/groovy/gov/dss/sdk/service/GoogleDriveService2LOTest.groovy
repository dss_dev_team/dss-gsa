package gov.dss.sdk.service

import gov.dss.sdk.service.module.TestGuiceModule
import org.springframework.core.io.ClassPathResource

import javax.inject.Inject

import static com.google.inject.Guice.createInjector
import static gov.dss.sdk.service.MessageService.*

/**
 * Test for GoogleDrive service
 *
 * Created by Carvel on 3/23/16 @ 7:11 PM.
 *
 */
class GoogleDriveService2LOTest extends GroovyTestCase {
    @Inject
    GoogleDriveService2LO googleDriveService2LO

    @Override
    protected void setUp() throws Exception {
        createInjector(new TestGuiceModule(this));
    }

    @Override
    protected void tearDown() throws Exception {
    }

    void testDependencyInjection() {
        assert googleDriveService2LO != null
    }

    void testSendMessageWithGoogleDriveService2LO() {
        //--- setup test data
        String fullyQualifiedFilename = new ClassPathResource("test.xlsx").getFile().getAbsolutePath()
        String directory = "directory"

        //--- exercise method
        MessageServiceResponse results = googleDriveService2LO.sendMessage([(ACTION): (UPLOAD), (FILE_NAME): fullyQualifiedFilename, (FOLDER): directory])

        //--- make assertions
        assert results.status == MessageServiceResponse.Ok.status
    }
}
