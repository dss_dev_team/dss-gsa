package gov.dss.sdk.service

/**
 * Author : Carvel 
 * Date   : 6/15/16
 *
 * Test case to exercise BaseMessageService
 *
 */
class BaseMessageServiceTest extends GroovyTestCase {

    void testNoActionDefined() {
        //--- create test data
        TestService testService = new TestService()
        def message = shouldFail {
            //--- exercise method
            testService.sendMessage([:])
        }
        //--- make assertions
        assert 'An action is required. Try one of the following actions [ACTION1, ACTION2].' == message
    }

    void testActionWithMissingParameter() {
        //--- create test data
        TestService testService = new TestService()
        def message = shouldFail {
            //--- exercise method
            testService.sendMessage(["action": "ACTION2", "PARAMETER1": "VALUE1"])
        }
        //--- make assertions
        assert 'For action [ACTION2], parameters [PARAMETER1, PARAMETER2] are required.' == message
    }

    void testInvalidAction() {
        //--- create test data
        TestService testService = new TestService()
        def message = shouldFail {
            //--- exercise method
            testService.sendMessage(["action": "BOGUS_ACTION"])
        }
        //--- make assertions
        assert 'Action [BOGUS_ACTION] is not supported. Try one of the following actions instead [ACTION1, ACTION2].' == message
    }

    void testInvalidParameter() {
        //--- create test data
        TestService testService = new TestService()
        def message = shouldFail {
            //--- exercise method
            testService.sendMessage(["action": "ACTION2", "BOGUS_PARAMETER": "BOGUS_VALUE"])
        }
        //--- make assertions
        println message
        assert 'For action [ACTION2], parameters [BOGUS_PARAMETER] is not supported. Try one of the following parameters instead [PARAMETER1, PARAMETER2].' == message
    }


    class TestService extends BaseMessageService {
        MessageServiceResponse onSendMessage(Map<String, Object> message) {
            MessageServiceResponse.Ok
        }

        def getSupportedActions() {
            ["ACTION1": ["PARAMETER1"], "ACTION2": ["PARAMETER1", "PARAMETER2"]]
        }
    }
}
