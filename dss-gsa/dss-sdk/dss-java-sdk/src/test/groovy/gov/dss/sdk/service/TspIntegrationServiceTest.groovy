package gov.dss.sdk.service

import gov.dss.sdk.service.module.TestGuiceModule
import org.apache.commons.io.FileUtils
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import javax.inject.Inject
import java.nio.file.Paths

import static com.google.inject.Guice.createInjector
import static gov.dss.sdk.service.MessageService.*

/**
 * Author : Carvel 
 * Date   : 6/15/16
 *
 * Test cases for the TspIntegrationService
 */
class TspIntegrationServiceTest extends GroovyTestCase {

    @Inject
    TspIntegrationService tspIntegrationService

    void setUp() {
        createInjector(new TestGuiceModule(this));
    }

    @Override
    protected void tearDown() throws Exception {
        MessageServiceResponse response = new LocalFileService().sendMessage([(ACTION): (FIND), (FILE_NAME): "tsp_spread_sheet.xlsx"])
        if ((response.payload as File) != null)
            FileUtils.forceDelete(response.payload as File)
    }

    void testDependencyInjection() {
        //--- make assertions
        assert tspIntegrationService != null
    }

    void testParseMessage() {
        //--- set up test data
        def filename = "GSA_TSP_AGREEMENT.pdf"
        MessageServiceResponse response = new LocalFileService().sendMessage([(ACTION): (FIND), (FILE_NAME): filename])
        File pdfFile = response.payload as File

        //--- exercise method
        MessageServiceResponse result = tspIntegrationService.sendMessage([(ACTION): (PARSE), (FILE): pdfFile])

        //--- make assertions
        assert result != null
        assert (result.payload as TspIntegrationService.TPIData).tradingPartner == "Freight Forwarder"
        assert (result.payload as TspIntegrationService.TPIData).socioEconomicStatus == "Veteran-Owned"
        assert (result.payload as TspIntegrationService.TPIData).sCacCode == "SCAC"
        assert (result.payload as TspIntegrationService.TPIData).companyName == "MyComp"
        assert (result.payload as TspIntegrationService.TPIData).address1 == "101 independence"
        assert (result.payload as TspIntegrationService.TPIData).address2 == "200"
        assert (result.payload as TspIntegrationService.TPIData).city == "sterling"
        assert (result.payload as TspIntegrationService.TPIData).state == "va"
        assert (result.payload as TspIntegrationService.TPIData).zipCode == "20165"
        assert (result.payload as TspIntegrationService.TPIData).country == "usa"
        assert (result.payload as TspIntegrationService.TPIData).phone1 == "999-999-9999"
        assert (result.payload as TspIntegrationService.TPIData).phone2 == "888-888-8888"
        assert (result.payload as TspIntegrationService.TPIData).fax == "777-777-7777"
        assert (result.payload as TspIntegrationService.TPIData).url == "mycomp.com"
        assert (result.payload as TspIntegrationService.TPIData).carrierInsuranceExpiryDate == "01/01/1990"
        assert (result.payload as TspIntegrationService.TPIData).carrierInsuranceAmount == "150"
        assert (result.payload as TspIntegrationService.TPIData).tender1FirstName == "john"
        assert (result.payload as TspIntegrationService.TPIData).tender1LastName == "doe"
        assert (result.payload as TspIntegrationService.TPIData).tender1Phone == "444-444-4444"
        assert (result.payload as TspIntegrationService.TPIData).tender1Email == "john@john.com"
        assert (result.payload as TspIntegrationService.TPIData).tender2FirstName == "bob"
        assert (result.payload as TspIntegrationService.TPIData).tender2LastName == "cat"
        assert (result.payload as TspIntegrationService.TPIData).tender2Phone == "333-333-3333"
        assert (result.payload as TspIntegrationService.TPIData).tender2Email == "222-222-2222"
    }

    void testGenerateExcelFile() {
        //--- set up test data
        TspIntegrationService.TPIData tpiData = new TspIntegrationService.TPIData()
        tpiData.tradingPartner = "Freight Forwarder"
        tpiData.socioEconomicStatus = "Veteran-Owned"
        tpiData.sCacCode = "SCAC"
        tpiData.companyName = "MyComp"
        tpiData.address1 = "101 independence"
        tpiData.address2 = "200"
        tpiData.city = "sterling"
        tpiData.state = "va"
        tpiData.zipCode = "20165"
        tpiData.country = "usa"
        tpiData.phone1 = "999-999-9999"
        tpiData.phone2 = "888-888-8888"
        tpiData.fax = "777-777-7777"
        tpiData.url = "mycomp.com"
        tpiData.carrierInsuranceExpiryDate = "01/01/1990"
        tpiData.carrierInsuranceAmount = "150"
        tpiData.tender1FirstName = "john"
        tpiData.tender1LastName = "doe"
        tpiData.tender1Phone = "444-444-4444"
        tpiData.tender1Email = "john@john.com"
        tpiData.tender2FirstName = "bob"
        tpiData.tender2LastName = "cat"
        tpiData.tender2Phone = "333-333-3333"
        tpiData.tender2Email = "bob@cat.com"

        //--- exercise code
        MessageServiceResponse result = tspIntegrationService.sendMessage([(ACTION): (GENERATE_EXCEL_FILE), (TPI_DATA): tpiData])

        //--- make assertions
        assert result.payload != null
        assert result.payload.class == File.class


        Paths.get('tsp_spread_sheet.xlsx').withInputStream { input ->
            def header = []
            def workbook = new XSSFWorkbook(input)
            def sheet = workbook.getSheetAt(0)

            for (cell in sheet.getRow(0).cellIterator()) {
                header << cell.stringCellValue
            }

            def headerFlag = true
            for (row in sheet.rowIterator()) {
                if (headerFlag) {
                    headerFlag = false
                    continue
                }
                for (cell in row.cellIterator()) {
                    switch (header[cell.columnIndex]) {
                        case 'Tsp Type':
                            assert cell.stringCellValue == 'Freight Forwarder'
                            break
                        case 'MWOB Type':
                            assert cell.stringCellValue == 'Veteran-Owned'
                            break
                        case 'SCACCode':
                            assert cell.stringCellValue == 'SCAC'
                            break
                        case 'CarrierDescription':
                            assert cell.stringCellValue == 'MyComp'
                            break
                        case 'Street':
                            assert cell.stringCellValue == '101 independence, 200'
                            break
                        case 'City':
                            assert cell.stringCellValue == 'sterling'
                            break
                        case 'State':
                            assert cell.stringCellValue == 'va'
                            break
                        case 'Postal Code':
                            assert cell.stringCellValue == '20165'
                            break
                        case 'Country':
                            assert cell.stringCellValue == 'usa'
                            break
                        case 'PrimaryTelephoneNumber':
                            assert cell.stringCellValue == '999-999-9999'
                            break
                        case 'SecondaryTelephoneNumber':
                            assert cell.stringCellValue == '888-888-8888'
                            break
                        case 'FaxNumber':
                            assert cell.stringCellValue == '777-777-7777'
                            break
                        case 'URL':
                            assert cell.stringCellValue == 'mycomp.com'
                            break
                        case 'CarrierInsuranceExpiryDate':
                            assert cell.stringCellValue == '01/01/1990'
                            break
                        case 'CarrierInsuranceAmount':
                            assert cell.stringCellValue == '150'
                            break
                        case 'Primary Contact FirstName LastName':
                            assert cell.stringCellValue == 'john doe'
                            break
                        case 'Primary Contact Telephone Number':
                            assert cell.stringCellValue == '444-444-4444'
                            break
                        case 'Primary Contact Email Address':
                            assert cell.stringCellValue == 'john@john.com'
                            break
                        case 'Secondary Contact FirstName LastName':
                            assert cell.stringCellValue == 'bob cat'
                            break
                        case 'Secondary Contact Telephone Number':
                            assert cell.stringCellValue == '333-333-3333'
                            break
                        case 'Secondary Contact Email Address':
                            assert cell.stringCellValue == 'bob@cat.com'
                            break
                        default:
                            fail('Unknown Column')
                            break
                    }
                }
            }
        }
    }

    void testGenerateExcelValidation() {
        //--- set up test data
        TspIntegrationService.TPIData tpiData = new TspIntegrationService.TPIData()
        tpiData.tradingPartner = "Freight Forwarder"
        tpiData.socioEconomicStatus = "Veteran-Owned"
        tpiData.sCacCode = "SCAC"
        tpiData.companyName = "MyComp"
        tpiData.address1 = "101 independence"
        tpiData.address2 = "200"
        tpiData.city = "sterling"
        tpiData.state = "YY"
        tpiData.zipCode = "1234"
        tpiData.country = "usa"
        tpiData.phone1 = "222 2222"
        tpiData.phone2 = "222 2222"
        tpiData.fax = "222 2222"
        tpiData.url = "mycomp.com"
        tpiData.carrierInsuranceExpiryDate = "Fri Jun 17 2:30pm"
        tpiData.carrierInsuranceAmount = "150"
        tpiData.tender1FirstName = "john"
        tpiData.tender1LastName = "doe"
        tpiData.tender1Phone = "222 2222"
        tpiData.tender1Email = "test@"
        tpiData.tender2FirstName = "bob"
        tpiData.tender2LastName = "cat"
        tpiData.tender2Phone = "222 2222"
        tpiData.tender2Email = "test@"


        MessageServiceResponse validatedTpiData = tspIntegrationService.sendMessage([(ACTION): (VALIDATE), (TPI_DATA): tpiData])

        //--- make assertions

        //--- exercise code
        MessageServiceResponse result = tspIntegrationService.sendMessage([(ACTION): (GENERATE_EXCEL_FILE), (TPI_DATA): validatedTpiData.payload])

        //--- make assertions
        assert result.payload != null
        assert result.payload.class == File.class


        Paths.get('tsp_spread_sheet.xlsx').withInputStream { input ->
            def header = []
            def workbook = new XSSFWorkbook(input)
            def sheet = workbook.getSheetAt(0)

            for (cell in sheet.getRow(0).cellIterator()) {
                header << cell.stringCellValue
            }

            def headerFlag = true
            for (row in sheet.rowIterator()) {
                if (headerFlag) {
                    headerFlag = false
                    continue
                }
                for (cell in row.cellIterator()) {
                    switch (header[cell.columnIndex]) {
                        case 'Tsp Type':
                            assert cell.stringCellValue == 'Freight Forwarder'
                            break
                        case 'MWOB Type':
                            assert cell.stringCellValue == 'Veteran-Owned'
                            break
                        case 'SCACCode':
                            assert cell.stringCellValue == 'SCAC'
                            break
                        case 'CarrierDescription':
                            assert cell.stringCellValue == 'MyComp'
                            break
                        case 'Street':
                            assert cell.stringCellValue == '101 independence, 200'
                            break
                        case 'City':
                            assert cell.stringCellValue == 'sterling'
                            break
                        case 'State':
                            assert cell.stringCellValue == 'State [YY] is invalid. Please use a valid two character state. Example  : MD'
                            break
                        case 'Postal Code':
                            assert cell.stringCellValue == 'Company zip code [1234] is invalid. Please use a 5 digit zip code: 55555'
                            break
                        case 'Country':
                            assert cell.stringCellValue == 'usa'
                            break
                        case 'PrimaryTelephoneNumber':
                            assert cell.stringCellValue == 'Company primary phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323'
                            break
                        case 'SecondaryTelephoneNumber':
                            assert cell.stringCellValue == 'Company secondary phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323'
                            break
                        case 'FaxNumber':
                            assert cell.stringCellValue == 'Company fax number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323'
                            break
                        case 'URL':
                            assert cell.stringCellValue == 'mycomp.com'
                            break
                        case 'CarrierInsuranceExpiryDate':
                            assert cell.stringCellValue == 'Carrier insurance expiry date [Fri Jun 17 2:30pm] is invalid. Please use the following format: MM/DD/YYYY'
                            break
                        case 'CarrierInsuranceAmount':
                            assert cell.stringCellValue == '150'
                            break
                        case 'Primary Contact FirstName LastName':
                            assert cell.stringCellValue == 'john doe'
                            break
                        case 'Primary Contact Telephone Number':
                            assert cell.stringCellValue == 'Primary tender contact phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323'
                            break
                        case 'Primary Contact Email Address':
                            assert cell.stringCellValue == 'Primary tender contact email [test@] is invalid. Please use a valid email address'
                            break
                        case 'Secondary Contact FirstName LastName':
                            assert cell.stringCellValue == 'bob cat'
                            break
                        case 'Secondary Contact Telephone Number':
                            assert cell.stringCellValue == 'Secondary tender contact phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323'
                            break
                        case 'Secondary Contact Email Address':
                            assert cell.stringCellValue == 'Secondary tender contact email [test@] is invalid. Please use a valid email address'
                            break
                        default:
                            fail('Unknown Column')
                            break
                    }
                }
            }
        }
    }

    void testValidateTpiData() {
        //--- set up test data
        TspIntegrationService.TPIData tpiData = new TspIntegrationService.TPIData()
        tpiData.zipCode = "1234"
        tpiData.state = "YY"
        tpiData.phone1 = "222 2222"
        tpiData.phone2 = "222 2222"
        tpiData.fax = "222 2222"
        tpiData.tender1FirstName = "tender1FirstName"
        tpiData.tender1LastName = "tender1LastName"
        tpiData.tender1Phone = "222 2222"
        tpiData.tender1Email = "test@"
        tpiData.tender2Phone = "222 2222"
        tpiData.tender2Email = "test@"
        tpiData.carrierInsuranceExpiryDate = "Fri Jun 17 2:30pm"

        //--- exercise methods
        MessageServiceResponse result = tspIntegrationService.sendMessage([(ACTION): (VALIDATE), (TPI_DATA): tpiData])

        //--- make assertions
        assert result.status == "Failed"
        assert result.message == "There was one or more validation errors"
        assert result.hasErrors() == true
        assert result.errors.contains("Company zip code [1234] is invalid. Please use a 5 digit zip code: 55555")
        assert result.errors.contains("State [YY] is invalid. Please use a valid two character state. Example  : MD")
        assert result.errors.contains("Company primary phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323")
        assert result.errors.contains("Company primary phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323")
        assert result.errors.contains("Company fax number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323")
        assert result.errors.contains("Primary tender contact phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323")
        assert result.errors.contains("Primary tender contact email [test@] is invalid. Please use a valid email address")
        assert result.errors.contains("Secondary tender contact phone number [222 2222] is invalid. Please use a valid phone number. Example 202-353-3323")
        assert result.errors.contains("Secondary tender contact email [test@] is invalid. Please use a valid email address")
        assert result.errors.contains("Carrier insurance expiry date [Fri Jun 17 2:30pm] is invalid. Please use the following format: MM/DD/YYYY")
    }

    void testRequiredTpiData() {
        //--- set up test data
        TspIntegrationService.TPIData tpiData = new TspIntegrationService.TPIData()
        tpiData.phone1 = ""
        tpiData.tender1FirstName = ""
        tpiData.tender1LastName = ""
        tpiData.tender1Phone = ""
        tpiData.tender1Email = ""

        //--- exercise methods
        MessageServiceResponse result = tspIntegrationService.sendMessage([(ACTION): (VALIDATE), (TPI_DATA): tpiData])

        //--- make assertions
        assert result.status == "Failed"
        assert result.message == "There was one or more validation errors"
        assert result.hasErrors() == true
        assert result.errors.contains("Company primary phone number is required.")
        assert result.errors.contains("Primary tender contact first name and last name are required.")
        assert result.errors.contains("Primary tender contact phone number is required.")
        assert result.errors.contains("Primary tender contact email is required.")
    }
}
