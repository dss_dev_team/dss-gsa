package gov.dss.sdk.service

import gov.dss.sdk.service.module.TestGuiceModule
import org.springframework.core.io.ClassPathResource

import javax.inject.Inject

import static com.google.inject.Guice.createInjector

/**
 *
 *
 *
 * Created by Carvel on 3/6/16 @ 12:56 PM.
 *
 */
class GmailServiceTest extends GroovyTestCase {

    @Inject
    GmailService gmailService

    void setUp() {
        createInjector(new TestGuiceModule(this));
    }

    void testDependencyInjection() {
        //--- make assertions
        assert gmailService != null
    }

    void testSendMessage() {
        //--- exercise method
        Map<String, String> message = new HashMap<>()
        message.put(MessageService.RECEIVER, "dssdeveloper.ch@gmail.com")
        message.put(MessageService.FROM, "dssteam.integration.test@gmail.com")
        message.put(MessageService.SUBJECT, "This is a test subject")
        message.put(MessageService.BODY, "This is a test body")
        message.put(MessageService.ATTACHMENT_FILE_NAME, new ClassPathResource("dsscr-251-GSA-TSP-Agreement.pdf").getFile().getAbsolutePath())

        MessageServiceResponse result = gmailService.sendMessage(message)
        //--- make assertions

        assert result == MessageServiceResponse.Ok
    }
}
