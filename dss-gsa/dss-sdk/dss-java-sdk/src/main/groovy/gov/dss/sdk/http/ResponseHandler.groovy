package gov.dss.sdk.http

/**
 * Interface to abstract the notion of handling a response where a response could be SOAP, Json,
 * or JMS, etc.
 *
 * Author : Carvel
 * Date   : 5/9/16
 *
 * @param < R >  - Response : type of response to return
 * @param < D >  - Data : data to inject into the returned response
 */
interface ResponseHandler<R, D> {
    /**
     * This method will generate and return response<R> using the passed in data<D>
     *
     * @param d
     * @return
     */
    R generate(D d)
}