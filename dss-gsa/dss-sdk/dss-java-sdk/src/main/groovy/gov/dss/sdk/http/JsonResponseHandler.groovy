package gov.dss.sdk.http

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper

/**
 * An implementation of ResponseHandler that generates a json string using data encapsulated in
 * a HashMap object.
 *
 * Please note that error message are injected from a property file
 *
 * Author : Carvel 
 * Date   : 5/9/16
 */
class JsonResponseHandler implements ResponseHandler<String, Map<String, Object>> {
    String errorMessages

    JsonResponseHandler(String errorMessages) {
        this.errorMessages = errorMessages
    }

    @Override
    String generate(Map<String, Object> data) {
        def slurper = new JsonSlurper()
        def errors = slurper.parseText(errorMessages)
        def response
        try {
            if (data.status != 200) {
                data = errors.find { it['status'] == data['status'] }
            }
            response = new ObjectMapper().writeValueAsString(data)
        } catch (Exception e) {
            e.printStackTrace();
        }
        response
    }
}
