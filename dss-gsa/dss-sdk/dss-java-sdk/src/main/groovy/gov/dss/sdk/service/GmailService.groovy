package gov.dss.sdk.service

import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax

import javax.activation.DataHandler
import javax.activation.DataSource
import javax.activation.FileDataSource
import javax.inject.Singleton
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

/**
 *
 * Service that sends an email message via Gmail smtp
 *
 * Created by Carvel on 3/6/16 @ 8:33 AM.
 *
 */
@Singleton
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class GmailService implements MessageService {
    @InjectConfig(value = "gmailUsername")
    private String username

    @InjectConfig(value = "gmailPassword")
    private String password

    @InjectConfig(value = "gmailSmtpAuth")
    private String smtpAuth

    @InjectConfig(value = "gmailSmtpStartTlsEnable")
    private String smtpStartTlsEnable

    @InjectConfig(value = "gmailSmtpHost")
    private String smtpHost

    @InjectConfig(value = "gmailSmtpPort")
    private String smtpPort

    @Override
    MessageServiceResponse sendMessage(Map<String, Object> email) {

        Properties props = new Properties()
        props.put("mail.smtp.auth", smtpAuth)
        props.put("mail.smtp.starttls.enable", smtpStartTlsEnable)
        props.put("mail.smtp.host", smtpHost)
        props.put("mail.smtp.port", smtpPort)

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(username, password)
            }
        })

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(email.get(MessageService.FROM)));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email.get(MessageService.RECEIVER)));

            // Set Subject: header field
            message.setSubject(email.get(MessageService.SUBJECT));

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(email.get(MessageService.BODY));

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = email.get(MessageService.ATTACHMENT_FILE_NAME)
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            println "=======> sent email [" + email + "]"

        } catch (javax.mail.MessagingException e) {
            throw new RuntimeException(e)
        }
        return MessageServiceResponse.Ok
    }


    @Override
    public String toString() {
        return "GmailService{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", smtpAuth='" + smtpAuth + '\'' +
                ", smtpStartTlsEnable='" + smtpStartTlsEnable + '\'' +
                ", smtpHost='" + smtpHost + '\'' +
                ", smtpPort='" + smtpPort + '\'' +
                '}';
    }
}
