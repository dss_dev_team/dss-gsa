package gov.dss.sdk.service

import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax
import org.apache.chemistry.opencmis.client.api.Folder
import org.apache.chemistry.opencmis.client.api.Repository
import org.apache.chemistry.opencmis.client.api.Session
import org.apache.chemistry.opencmis.client.api.SessionFactory
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId
import org.apache.chemistry.opencmis.commons.enums.BindingType
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException
import org.apache.tika.Tika

import javax.inject.Singleton

import static org.apache.chemistry.opencmis.commons.PropertyIds.NAME
import static org.apache.chemistry.opencmis.commons.PropertyIds.OBJECT_TYPE_ID
import static org.apache.chemistry.opencmis.commons.SessionParameter.*

/**
 *
 * An implementation of a MessageService that integrates with Alfresco
 *
 * Created by Carvel on 4/25/16 @ 9:15 AM.
 *
 */
@Singleton
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class AlfrescoService extends BaseMessageService {
    @InjectConfig(value = "alfrescoUsername")
    private String username

    @InjectConfig(value = "alfrescoPassword")
    private String password

    @InjectConfig(value = "alfrescoIacpPath")
    private String path

    @InjectConfig(value = "alfrescoIacpAtomPubUrl")
    private String atomPubUrl

    MessageServiceResponse onSendMessage(Map<String, Object> message) {
        MessageServiceResponse response
        if (message.get(ACTION).equals(UPLOAD)) {
            response = upload(message.get(FILE))
        } else if (message.get(ACTION).equals(DELETE)) {
            response = delete(message.get(FILE_NAME))
        }
        response
    }

    def getSupportedActions() {
        [(UPLOAD): [FILE], (DELETE): [FILE_NAME]]
    }

    MessageServiceResponse delete(String filename) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        Session session = getSession()
        try {
            def document = session.getObjectByPath(path + filename)
            document.delete(true)
        } catch (CmisObjectNotFoundException ex) {
            response = MessageServiceResponse.Failed
            response.addError(ex.message)
            response.message = ex.message
        }
        response
    }

    MessageServiceResponse upload(File file) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        Session session = getSession()
        Folder folder = session.getObjectByPath(path)

        Tika tika = new Tika()
        String mimeType = tika.detect(file)
        def contentStream = session.getObjectFactory().createContentStream(file.name, file.size(), mimeType, new FileInputStream(file))
        try {
            def props = [(OBJECT_TYPE_ID): (BaseTypeId.CMIS_DOCUMENT.value()), (NAME): file.name]
            response.payload = folder.createDocument(props, contentStream, null)
        } catch (CmisContentAlreadyExistsException ex) {
            /**
             * rename file if it already exists in Alfresco
             */
            String newFilename = file.name.substring(0, file.name.lastIndexOf('.'));
            java.time.format.DateTimeFormatter timeStampPattern = java.time.format.DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            newFilename = newFilename.concat(".").concat(timeStampPattern.format(java.time.LocalDateTime.now())).concat(".pdf");
            def props = [(OBJECT_TYPE_ID): (BaseTypeId.CMIS_DOCUMENT.value()), (NAME): newFilename]
            response.payload = folder.createDocument(props, contentStream, null)
        }

        response
    }

    Session getSession() {
        // Create a SessionFactory and set up the SessionParameter map
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance()

        // using the AtomPUB binding
        def parameter = [:]
        parameter.put(USER, username)
        parameter.put(PASSWORD, password)
        parameter.put(ATOMPUB_URL, atomPubUrl)
        parameter.put(BINDING_TYPE, BindingType.ATOMPUB.value())

        // find all the repositories at this URL - there should only be one.
        List<Repository> repositories = sessionFactory.getRepositories(parameter)

        // create session with the first (and only) repository
        Repository repository = repositories.get(0)
        parameter.put(REPOSITORY_ID, repository.getId())
        sessionFactory.createSession(parameter)
    }
}
