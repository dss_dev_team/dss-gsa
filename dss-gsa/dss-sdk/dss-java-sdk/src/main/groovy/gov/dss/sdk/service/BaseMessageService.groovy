package gov.dss.sdk.service

/**
 * Author : Carvel 
 * Date   : 6/15/16
 *
 * Base class for MessageService implementations
 */
abstract class BaseMessageService implements MessageService {
    MessageServiceResponse sendMessage(Map<String, Object> message) {
        //--- validate message
        if (message.action == null)
            throw new IllegalArgumentException("An action is required. Try one of the following actions ${supportedActions.keySet()}.")

        if (supportedActions[message.action] == null) {
            throw new IllegalArgumentException("Action [${message.action}] is not supported. Try one of the following actions instead ${supportedActions.keySet()}.")
        }

        if (!supportedActions[message.action].intersect(message.keySet())) {
            throw new IllegalArgumentException("For action [${message.action}], parameters [${message.find { it.key != 'action' }.key}] is not supported. Try one of the following parameters instead ${supportedActions[message.action]}.")
        }

        if (message.keySet().toArray()[1..message.keySet().toArray().size() - 1] != supportedActions[message.action]) {
            throw new IllegalArgumentException("For action [${message.action}], parameters ${supportedActions[message.action]} are required.")
        }
        println "=====> message sent: $message"
        MessageServiceResponse response = onSendMessage(message)
        println "<=====message response: $response"
        response
    }

    /**
     * required sendMessage  call
     * @param message
     * @return
     */
    abstract MessageServiceResponse onSendMessage(Map<String, Object> message)

    /**
     * a list of supported actions
     * @return
     */
    abstract def getSupportedActions()

}
