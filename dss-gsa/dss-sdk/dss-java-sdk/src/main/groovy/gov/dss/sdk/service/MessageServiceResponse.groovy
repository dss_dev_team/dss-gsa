package gov.dss.sdk.service

/**
 *
 * Simple Abstraction for a message response
 *
 * Created by Carvel on 3/6/16 @ 8:28 AM.
 *
 */
class MessageServiceResponse {
    public static final Ok = new MessageServiceResponse("Ok");
    public static final Failed = new MessageServiceResponse("Failed");

    String status;
    String message;
    Object payload;

    public MessageServiceResponse(String status) {
        this.status = status
    }

    List<String> errors = new ArrayList<String>()

    void addError(String error) {
        errors.add(error)
    }

    boolean hasErrors() {
        return errors.size() > 0
    }

    @Override
    public String toString() {
        return "MessageServiceResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", payload=" + payload +
                ", errors=" + errors +
                '}';
    }
}
