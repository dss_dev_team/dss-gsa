/*
 * Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package gov.dss.sdk.service;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;

import java.io.IOException;

/**
 * This class manages the status/progress of a file upload process. It can be used to send feedback back to the user
 * for cases when we are uploading very large files.
 */
public class FileUploadProgressListener implements MediaHttpUploaderProgressListener {

    public void progressChanged(MediaHttpUploader uploader) throws IOException {
        switch (uploader.getUploadState()) {
            case INITIATION_STARTED:
                System.out.println("file upload init started");
                break;
            case INITIATION_COMPLETE:
                System.out.println("file upload inti complete");
                break;
            case MEDIA_IN_PROGRESS:
                System.out.println("file upload in progress");
                break;
            case MEDIA_COMPLETE:
                System.out.println("file upload in complete");
                break;
        }
    }
}
