package gov.dss.sdk.annotations

import com.google.inject.BindingAnnotation

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Author : Carvel 
 * Date   : 4/25/16
 */
@BindingAnnotation
@Target([ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@interface AlfrescoServiceAnnotation {
}