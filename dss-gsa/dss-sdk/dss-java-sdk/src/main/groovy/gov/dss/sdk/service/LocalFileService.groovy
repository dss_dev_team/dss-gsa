package gov.dss.sdk.service

import org.springframework.core.io.ClassPathResource

/**
 *
 * This implementation of MessageService will return a file from the local hard drive. It
 *
 * Created by Carvel on 4/4/16 @ 3:35 PM.
 *
 */
class LocalFileService extends BaseMessageService {

    MessageServiceResponse onSendMessage(Map<String, Object> message) {
        MessageServiceResponse response
        if (message.get(ACTION).equals(FIND)) {
            response = find(message.get(FILE_NAME))
        }
        response
    }

    def getSupportedActions() {
        [(FIND): [FILE_NAME]]
    }

    MessageServiceResponse find(String filename) {

        MessageServiceResponse response = MessageServiceResponse.Ok
        try {
            java.io.File file = null
            if (new java.io.File((String) filename).exists()) {
                file = new java.io.File(filename)
            } else {
                file = new ClassPathResource(filename).getFile()
            }
            if (file == null) {
                response.status = MessageServiceResponse.Failed
                response.message = this.class.name + " could not locate $filename on the local filesystem"
                response.addError(response.getMessage())
            } else {
                response.status = MessageServiceResponse.Ok
                response.payload = file
            }
        } catch (Exception ex) {
            response.status = MessageServiceResponse.Failed
            response.message = this.class.name + " could not locate $filename on the local filesystem"
            response.payload = null
            response.addError(response.getMessage())
        }
        response
    }

    /**
     * DSSCR-438 added helper methods to save bytes to disk
     *
     * @param content
     * @param file
     */
    public static void saveTo(byte[] content, String file) {
        saveTo(content, new File(file))
    }

    public static void saveTo(byte[] content, File file) {
        if (file == null) {
            throw new IllegalArgumentException("file argument cannot be null")
        }

        FileOutputStream out = null

        try {
            out = new FileOutputStream(file)
            out.write(content)
        } finally {
            close(out)
        }
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close()
            } catch (IOException ignored) {
            }
        }
    }

}
