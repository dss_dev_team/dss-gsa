package gov.dss.sdk.service

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.FileContent
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.File
import com.google.api.services.drive.model.ParentReference
import com.google.inject.Inject
import gov.dss.sdk.annotations.LocalFileServiceAnnotation
import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax
import org.apache.tika.Tika

/**
 * This implementation of the MessageService interface simple copies a file to a google drive using the 2-Legged
 * OAuth implementation of GoogleDrive API access.
 * http://cakebaker.42dh.com/2011/01/10/2-legged-vs-3-legged-oauth/
 *
 * Author : Carvel 
 * Date   : 3/9/16
 */
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class GoogleDriveService2LO extends BaseMessageService {
    final private JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance()

    @InjectConfig(value = "g2LoAccountEmail")
    String accountEmail

    @InjectConfig(value = "g2LoUserEmail")
    String userEmail

    @InjectConfig(value = "g2LoP12File")
    String p12File

    @InjectConfig(value = "g2ApplicationName")
    String applicationName

    Drive drive

    HttpTransport httpTransport

    @Inject
    @LocalFileServiceAnnotation
    MessageService localFileService

    private GoogleCredential authorize() {
        MessageServiceResponse response = localFileService.sendMessage([(ACTION): FIND, (FILE_NAME): p12File])
        java.io.File _p12File = response.payload as java.io.File

        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(JSON_FACTORY)
                .setServiceAccountId(accountEmail)
                .setServiceAccountPrivateKeyFromP12File(_p12File)
                .setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE))
                .setServiceAccountUser(userEmail)
                .build()
        return credential
    }


    MessageServiceResponse onSendMessage(Map<String, Object> message) {
        MessageServiceResponse response
        if (message.get(ACTION).equals(UPLOAD)) {
            response = upload(message)
        } else if (message.get(ACTION).equals(DELETE)) {
            response = delete(message)
        }
        response
    }

    def getSupportedActions() {
        [(UPLOAD): [FILE_NAME, FOLDER], (DELETE): [FILE_NAME]]
    }

    MessageServiceResponse upload(def message) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport()
            GoogleCredential credential = authorize()
            drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(applicationName).build()
            uploadFile(message[FILE_NAME], message[FOLDER])
        } catch (IOException e) {
            response = MessageServiceResponse.Failed
            response.addError(e.getMessage())
            e.printStackTrace()
        }
        println "send status: ${response.status} ${(response.status == MessageServiceResponse.Failed ? response.getErrors().toString() : "")}"
        return response
    }

    MessageServiceResponse delete(def message) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport()
            GoogleCredential credential = authorize()
            drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(applicationName).build()
            String fileId = drive.files().list().setQ("title='" + message[FILE_NAME]).execute()['items']['id'][0]
            drive.files().delete()
        } catch (IOException e) {
            response = MessageServiceResponse.Failed
            response.addError(e.getMessage())
            e.printStackTrace()
        }
        response
    }

    /**
     *
     * @param folder - name of the folder
     * @return folderId of the passed in folder name
     */
    String getFolderId(String folder) {
        String folderId = drive.files().list().setQ("title='" + folder + "' and mimeType='application/vnd.google-apps.folder'").execute()['items']['id'][0]
        if (null == folderId) {
            throw new RuntimeException("DssSDKError: [$folder] does not exist on google drive")
        }
        folderId
    }

    private void uploadFile(String uploadFilename, String folder) throws IOException {
        /**
         * get parentFolderId
         */
        String parentFolderId = getFolderId(folder)

        java.io.File file = new java.io.File(uploadFilename)
        /**
         * calculate file mime type
         */
        Tika tika = new Tika()
        String mimeType = tika.detect(file)
        File fileMetadata = new File()
        fileMetadata.setTitle(file.name)
        fileMetadata.setParents(Arrays.asList(new ParentReference().setId(parentFolderId)))
        FileContent mediaContent = new FileContent(mimeType, file)
        drive.files().insert(fileMetadata, mediaContent).execute()
    }
}
