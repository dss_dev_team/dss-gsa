package gov.dss.sdk.annotations

import com.google.inject.BindingAnnotation

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 *
 * A binding annotation used exclusively for dependency inject via Google Guice
 *
 * Created by Carvel on 4/4/16 @ 3:46 PM.
 *
 */
@BindingAnnotation
@Target([ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@interface LocalFileServiceAnnotation {

}