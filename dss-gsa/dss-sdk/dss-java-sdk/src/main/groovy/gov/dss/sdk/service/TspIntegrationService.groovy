package gov.dss.sdk.service

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.google.inject.Inject
import gov.dss.sdk.annotations.GoogleDriveServiceAnnotation
import gov.dss.sdk.excelbuilder.ExcelBuilder
import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax
import org.apache.commons.lang3.StringUtils
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDDocumentCatalog
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm
import org.apache.pdfbox.pdmodel.interactive.form.PDField

import javax.inject.Singleton
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.regex.Pattern

/**
 * Author : Carvel 
 * Date   : 6/15/16
 *
 * A MessageService implementation that parses a TSP Agreement PDF
 */
@Singleton
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class TspIntegrationService extends BaseMessageService {

    @InjectConfig(value = "tspSpreadSheetFilename")
    String tspSpreadSheetFilename

    @InjectConfig(value = "tspCopySpreadsheetToGoogleDrive")
    String tspCopySpreadsheetToGoogleDrive

    @Inject
    @GoogleDriveServiceAnnotation
    MessageService googleDriveService

    @InjectConfig(value = "tspGoogleDriveFolder")
    String googleDriveFolderName

    String message = "There was one or more validation errors"

    String zipRegex = '^[0-9]{5}(?:-[0-9]{4})?$';
    String zipError = "%s [%s] is invalid. Please use a 5 digit zip code: 55555"
    Pattern zipPattern = Pattern.compile(zipRegex);

    String phoneError = "%s [%s] is invalid. Please use a valid phone number. Example 202-353-3323"

    String emailRegex = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
    String emailError = "%s [%s] is invalid. Please use a valid email address"
    Pattern emailPattern = Pattern.compile(emailRegex);


    /**
     * valid state code collection
     */
    def abbreviations =
            ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN",
             "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS",
             "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI",
             "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    String dateError = "%s [%s] is invalid. Please use the following format: MM/DD/YYYY"

    MessageServiceResponse onSendMessage(Map<String, Object> message) {
        MessageServiceResponse response
        if (message.get(ACTION).equals(PARSE)) {
            response = parse(message.get(FILE))
        } else if (message.get(ACTION).equals(GENERATE_EXCEL_FILE)) {
            response = generateExcelFile(message.get(TPI_DATA))
        } else if (message.get(ACTION).equals(VALIDATE)) {
            response = validate(message.get(TPI_DATA))
        }
        response
    }

    def getSupportedActions() {
        [(PARSE): [FILE], (GENERATE_EXCEL_FILE): [TPI_DATA], (VALIDATE): [TPI_DATA]]
    }


    MessageServiceResponse validate(TPIData tpiData) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        /**
         * required field validation
         */
        requiredFieldValidation(tpiData, response)
        if (response.status == "Failed")
            return response
        /**
         * Field validation
         */
        if (StringUtils.isNotBlank(tpiData.zipCode) && !zipPattern.matcher(tpiData.zipCode)) {
            response.status = "Failed"
            response.message = message
            response.addError(String.format(zipError, "Company zip code", tpiData.zipCode))
            tpiData.zipCode = String.format(zipError, "Company zip code", tpiData.zipCode)
        }

        if (StringUtils.isNotBlank(tpiData.state) && !abbreviations.contains(tpiData.state)) {
            response.status = "Failed"
            response.message = message
            response.addError("State [${tpiData.state}] is invalid. Please use a valid two character state. Example  : MD")
            tpiData.state = "State [${tpiData.state}] is invalid. Please use a valid two character state. Example  : MD"
        }


        tpiData.phone1 = validatePhone(tpiData.phone1, response, "Company primary phone number")
        tpiData.phone2 = validatePhone(tpiData.phone2, response, "Company secondary phone number")
        tpiData.fax = validatePhone(tpiData.fax, response, "Company fax number")
        tpiData.tender1Phone = validatePhone(tpiData.tender1Phone, response, "Primary tender contact phone number")
        tpiData.tender2Phone = validatePhone(tpiData.tender2Phone, response, "Secondary tender contact phone number")

        if (StringUtils.isNotBlank(tpiData.tender1Email) && !emailPattern.matcher(tpiData.tender1Email)) {
            response.status = "Failed"
            response.message = message
            response.addError(String.format(emailError, "Primary tender contact email", tpiData.tender1Email))
            tpiData.tender1Email = String.format(emailError, "Primary tender contact email", tpiData.tender1Email)
        }

        if (StringUtils.isNotBlank(tpiData.tender2Email) && !emailPattern.matcher(tpiData.tender2Email)) {
            response.status = "Failed"
            response.message = message
            response.addError(String.format(emailError, "Secondary tender contact email", tpiData.tender2Email))
            tpiData.tender2Email = String.format(emailError, "Secondary tender contact email", tpiData.tender2Email)
        }

        if (StringUtils.isNotBlank(tpiData.carrierInsuranceExpiryDate) && !isDateValid(tpiData.carrierInsuranceExpiryDate)) {
            response.status = "Failed"
            response.message = message
            response.addError(String.format(dateError, "Carrier insurance expiry date", tpiData.carrierInsuranceExpiryDate))
            tpiData.carrierInsuranceExpiryDate = String.format(dateError, "Carrier insurance expiry date", tpiData.carrierInsuranceExpiryDate)
        }
        response.payload = tpiData
        response
    }

    String validatePhone(String phone, MessageServiceResponse response, String messagePrefix) {
        if (StringUtils.isEmpty(phone))
            return ""
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneProto = phoneUtil.parse(phone, "US");
            if (StringUtils.isNotBlank(phone) && !phoneUtil.isValidNumber(phoneProto)) {
                response.status = "Failed"
                response.message = message
                response.addError(String.format(phoneError, messagePrefix, phone))
                phone = String.format(phoneError, messagePrefix, phone)
            }
        } catch (NumberParseException e) {
            response.status = "Failed"
            response.message = message
            response.addError(String.format(phoneError, messagePrefix, phone))
            phone = String.format(phoneError, messagePrefix, phone)
        }
        phone
    }


    void requiredFieldValidation(TPIData tpiData, MessageServiceResponse response) {
        if (StringUtils.isBlank(tpiData.phone1)) {
            response.status = "Failed"
            response.message = message
            response.addError("Company primary phone number is required.")
            tpiData.phone1 = "Company primary phone number is required."
        }

        if (StringUtils.isBlank(tpiData.tender1FirstName) || StringUtils.isBlank(tpiData.tender1LastName)) {
            response.status = "Failed"
            response.message = message
            response.addError("Primary tender contact first name and last name are required.")
            tpiData.tender1FirstName = "Primary tender contact first name and last name are required."
            tpiData.tender1LastName = "Primary tender contact last name and last name are required."
        }

        if (StringUtils.isBlank(tpiData.tender1Phone)) {
            response.status = "Failed"
            response.message = message
            response.addError("Primary tender contact phone number is required.")
            tpiData.tender1Phone = "Primary tender contact phone number is required."
        }

        if (StringUtils.isBlank(tpiData.tender1Email)) {
            response.status = "Failed"
            response.message = message
            response.addError("Primary tender contact email is required.")
            tpiData.tender1Email = "Primary tender contact email is required."
        }

        response.payload = tpiData
    }

    public static boolean isDateValid(String text) {
        if (text == null || !text.matches("((0[1-9])|(1[0-2]))[\\/-]((0[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))[\\/-](\\d{4})"))
            return false;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        try {
            df.parse(text);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

    MessageServiceResponse generateExcelFile(TPIData tpiData) {
        MessageServiceResponse response = MessageServiceResponse.Ok

        def builder = new ExcelBuilder()
        builder {
            sheet {
                row(['Tsp Type', 'MWOB Type', 'SCACCode', 'CarrierDescription', 'Street',
                     'City', 'State', 'Postal Code', 'Country', 'PrimaryTelephoneNumber', 'SecondaryTelephoneNumber',
                     'FaxNumber', 'URL', 'CarrierInsuranceExpiryDate', 'CarrierInsuranceAmount',
                     'Primary Contact FirstName LastName', 'Primary Contact Telephone Number',
                     'Primary Contact Email Address', 'Secondary Contact FirstName LastName',
                     'Secondary Contact Telephone Number', 'Secondary Contact Email Address'])
                row([tpiData.tradingPartner, tpiData.socioEconomicStatus, tpiData.sCacCode, tpiData.companyName,
                     (StringUtils.isNotBlank(tpiData.address2) ? tpiData.address1 + ", " + tpiData.address2 : tpiData.address1),
                     tpiData.city, tpiData.state, tpiData.zipCode, tpiData.country, tpiData.phone1, tpiData.phone2,
                     tpiData.fax, tpiData.url, tpiData.carrierInsuranceExpiryDate, tpiData.carrierInsuranceAmount,
                     tpiData.tender1FirstName + " " + tpiData.tender1LastName, tpiData.tender1Phone, tpiData.tender1Email,
                     tpiData.tender2FirstName + " " + tpiData.tender2LastName, tpiData.tender2Phone, tpiData.tender2Email
                ])
            }
        }

        File excelFile = new File(tspSpreadSheetFilename as String)
        builder.workbook.write(new FileOutputStream(excelFile))

        response.payload = excelFile

        if (Boolean.parseBoolean(tspCopySpreadsheetToGoogleDrive)) {
            googleDriveService.sendMessage([(ACTION): (UPLOAD), (FILE_NAME): tspSpreadSheetFilename as String, (FOLDER): googleDriveFolderName])
        }

        response
    }

    MessageServiceResponse parse(File file) {
        MessageServiceResponse response = MessageServiceResponse.Ok
        TPIData tpiData = new TPIData()

        PDDocument pdDoc = PDDocument.load(file);
        PDDocumentCatalog pdCatalog = pdDoc.getDocumentCatalog();
        PDAcroForm pdAcroForm = pdCatalog.getAcroForm();

        try {
            for (PDField pdField : pdAcroForm.getFields()) {
                if (pdField.getFullyQualifiedName().equals("CHECKBOX|ULPsKGDMxaUP") && pdField.getValueAsString().equals("X")) {
                    tpiData.tradingPartner = "Freight Common Carrier"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|RSngnX40BuMQ") && pdField.getValueAsString().equals("X")) {
                    tpiData.tradingPartner = "Freight Forwarder"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|WhUlbg2ajgAU") && pdField.getValueAsString().equals("X")) {
                    tpiData.tradingPartner = "Freight Broker"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|YKx9mOX0NfgX") && pdField.getValueAsString().equals("X")) {
                    tpiData.tradingPartner = "Rate Filing Service Provider"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|CGrDIawD9LoG") && pdField.getValueAsString().equals("X")) {
                    tpiData.socioEconomicStatus = "Minority-Owned (Other than Woman-Owned)"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|XqXYjpBacZY7") && pdField.getValueAsString().equals("X")) {
                    tpiData.socioEconomicStatus = "Woman-Owned"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|kETusln9M8cR") && pdField.getValueAsString().equals("X")) {
                    tpiData.socioEconomicStatus = "Both (Minority and Woman Owned)"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|aUES823uDrAZ") && pdField.getValueAsString().equals("X")) {
                    tpiData.socioEconomicStatus = "Veteran-Owned"
                } else if (pdField.getFullyQualifiedName().equals("CHECKBOX|ES5m8dTMge8J") && pdField.getValueAsString().equals("X")) {
                    tpiData.socioEconomicStatus = "Service-Disabled Veteran-Owned"
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|iX2khAEHzOgK")) {
                    tpiData.sCacCode = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|MZXrwkW4VgcX")) {
                    tpiData.companyName = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|tlq25kqfoKI5")) {
                    tpiData.address1 = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|HzX90Z318Yo7")) {
                    tpiData.address2 = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|KxiPQBgObJYK")) {
                    tpiData.city = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|XFShqVyVTbIT")) {
                    tpiData.state = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|BgofbBy2nXQM")) {
                    tpiData.zipCode = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|7S9NNYMxks4M")) {
                    tpiData.country = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|ptIlPrIZ478I")) {
                    tpiData.phone1 = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|k2EEf9IWS802")) {
                    tpiData.phone2 = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|VlFt0MMh1Q0M")) {
                    tpiData.fax = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|WMnldOda58Q8")) {
                    tpiData.url = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|EVsjh7os4OwZ")) {
                    tpiData.carrierInsuranceExpiryDate = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|JNytOaDlWq00")) {
                    tpiData.carrierInsuranceAmount = pdField.getValueAsString()
                } else if (StringUtils.isNotEmpty(pdField.getValueAsString()) && pdField.getFullyQualifiedName().equals("TEXTFIELD|86DWXXm6qNEJ")) {
                    def name = pdField.getValueAsString().split(" ")
                    tpiData.tender1FirstName = name[0]
                    tpiData.tender1LastName = name[1]
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|Syxnr57bJGgJ")) {
                    tpiData.tender1Phone = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|07GeOtW7VwkW")) {
                    tpiData.tender1Email = pdField.getValueAsString()
                } else if (StringUtils.isNotEmpty(pdField.getValueAsString()) && pdField.getFullyQualifiedName().equals("TEXTFIELD|gyd4rErwXvsV")) {
                    def name = pdField.getValueAsString().split(" ")
                    tpiData.tender2FirstName = name[0]
                    tpiData.tender2LastName = name[1]
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|t1pdaSOe3YsE")) {
                    tpiData.tender2Phone = pdField.getValueAsString()
                } else if (pdField.getFullyQualifiedName().equals("TEXTFIELD|BHpou3to20oF")) {
                    tpiData.tender2Email = pdField.getValueAsString()
                }
            }
            response.status = MessageServiceResponse.Ok
            response.payload = tpiData

        }
        catch (Exception e) {
            e.printStackTrace();
            response.status = MessageServiceResponse.Failed
            response.message = this.class.name + " error parsing ${file.name}"
            response.addError(response.getMessage())
        } finally {
            pdDoc.close()
        }
        response
    }

    public static class TPIData {
        String tradingPartner
        String socioEconomicStatus
        String sCacCode
        String companyName
        String address1
        String address2
        String city
        String state
        String zipCode
        String country
        String phone1 //--- required
        String phone2
        String fax
        String url
        String carrierInsuranceExpiryDate
        String carrierInsuranceAmount
        String tender1FirstName //--- required
        String tender1LastName //--- required
        String tender1Phone //--- required
        String tender1Email //--- required
        String tender2FirstName
        String tender2LastName
        String tender2Phone
        String tender2Email

        @Override
        public String toString() {
            return "TPIData{" +
                    "tradingPartner='" + tradingPartner + '\'' +
                    ", socioEconomicStatus='" + socioEconomicStatus + '\'' +
                    ", sCacCode='" + sCacCode + '\'' +
                    ", companyName='" + companyName + '\'' +
                    ", address1='" + address1 + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", zipCode='" + zipCode + '\'' +
                    ", country='" + country + '\'' +
                    ", phone1='" + phone1 + '\'' +
                    ", phone2='" + phone2 + '\'' +
                    ", fax='" + fax + '\'' +
                    ", url='" + url + '\'' +
                    ", carrierInsuranceExpiryDate='" + carrierInsuranceExpiryDate + '\'' +
                    ", carrierInsuranceAmount='" + carrierInsuranceAmount + '\'' +
                    ", tender1FirstName='" + tender1FirstName + '\'' +
                    ", tender1LastName='" + tender1LastName + '\'' +
                    ", tender1Phone='" + tender1Phone + '\'' +
                    ", tender1Email='" + tender1Email + '\'' +
                    ", tender2FirstName='" + tender2FirstName + '\'' +
                    ", tender2LastName='" + tender2LastName + '\'' +
                    ", tender2Phone='" + tender2Phone + '\'' +
                    ", tender2Email='" + tender2Email + '\'' +
                    '}';
        }
    }
}
