package gov.dss.sdk.service;

import java.util.Map;

/**
 * A simple abstraction for message responses
 * <p/>
 * Created by Carvel on 3/6/16 @ 8:23 AM.
 */
public interface MessageService {
    /**
     * parameters
     */
    String RECEIVER = "receiver";
    String FROM = "from";
    String SUBJECT = "subject";
    String BODY = "body";
    String ATTACHMENT_FILE_NAME = "attachmentFilename";
    String FILE_NAME = "filename";
    String FOLDER = "folder";
    String FILE = "file";
    String TPI_DATA = "tpi_data";

    /**
     * Actions
     */
    String ACTION = "action";
    String UPLOAD = "upload";
    String DELETE = "delete";
    String FIND = "find";
    String PARSE = "parse";
    String GENERATE_EXCEL_FILE = "generateExcelFile";
    String VALIDATE = "validate";


    MessageServiceResponse sendMessage(Map<String, Object> message);
}
