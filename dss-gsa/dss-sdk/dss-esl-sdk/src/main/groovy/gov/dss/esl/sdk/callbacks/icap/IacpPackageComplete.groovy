package gov.dss.esl.sdk.callbacks.icap

import com.google.inject.Inject
import com.silanis.esl.sdk.Document
import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.PackageId
import com.silanis.esl.sdk.io.Files
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.callbacks.BaseCallback
import gov.dss.sdk.service.MessageServiceResponse

import static gov.dss.sdk.service.MessageService.*

/**
 * Author : Carvel 
 * Date   : 4/26/16
 */
class IacpPackageComplete extends BaseCallback {

    @Inject
    IacpPackageComplete(DssEslClient dssEslClient) {
        super(dssEslClient)
    }

    DocumentPackage handle() {
        DocumentPackage documentPackage = dssEslClient.getPackage(new PackageId(packageId))
        Document document = documentPackage.documents[1]
        String documentName = document.getName() + ".pdf"
        byte[] bytes = dssEslClient.downloadDocument(documentPackage.id, document.id.toString())
        Files.saveTo(bytes, documentName)
        MessageServiceResponse response = dssEslClient.localFileService.sendMessage([(ACTION): FIND, (FILE_NAME): documentName])
        File file = response.payload as File
        dssEslClient.alfrescoService.sendMessage([(ACTION): (UPLOAD), (FILE): file])
        documentPackage
    }
}
