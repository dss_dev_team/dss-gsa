package gov.dss.esl.sdk.callbacks

import com.silanis.esl.sdk.DocumentPackage
import gov.dss.esl.sdk.DssEslClient

/**
 * DSSCR-155 Notification of package ready for completion.
 *
 * Abstraction for handling  events
 *
 * Developer notes: This interface takes on the role of the "Command" in the "Command" Pattern
 *
 * Created by Carvel on 1/27/16.
 */
interface Callback {
    /**
     * Reference to eSignLive API
     * @return
     */
    DssEslClient getDssEslClient()
    /**
     * Handles a callback from eSignLive
     * @return the associated signing ceremony package
     */
    DocumentPackage handle()
    /**
     * Inject's packageId into all Callbacks
     */
    void setPackageId(String packageId)
}