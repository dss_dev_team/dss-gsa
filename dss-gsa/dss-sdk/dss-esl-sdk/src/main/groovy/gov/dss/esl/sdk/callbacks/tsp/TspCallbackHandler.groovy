package gov.dss.esl.sdk.callbacks.tsp

import com.silanis.esl.sdk.DocumentPackage
import gov.dss.esl.sdk.annontations.SendEmailOnEventCallbackAnnotation
import gov.dss.esl.sdk.annontations.TspPackageCompleteCallbackAnnotation
import gov.dss.esl.sdk.callbacks.*
import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax
import org.apache.commons.lang3.Validate

import javax.inject.Inject

/**
 *
 * This implementation of the CallbackHandler interface is for TSP specific events
 *
 * Created by Carvel on 1/27/16.
 */
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class TspCallbackHandler extends BaseCallbackHandler {
    @Inject
    @TspPackageCompleteCallbackAnnotation
    Callback tspPackageCompleteCb

    @Inject
    @SendEmailOnEventCallbackAnnotation
    Callback sendEmailOnEventCb

    @InjectConfig(value = "tspSendEmailToPackageOwnerOnPackageAttach")
    String tspSendEmailToPackageOwnerOnPackageAttach

    @InjectConfig(value = "tspEmailSender")
    String fromEmail

    @InjectConfig(value = "tspSendEmailToPackageOwnerOnPackageAttachBody")
    String packageAttachEmailBody

    @InjectConfig(value = "tspSendEmailToPackageOwnerOnPackageAttachSubject")
    String packageAttachEmailSubject

    @InjectConfig(value = "tspGoogleDriveFolder")
    String googleDriveFolderName


    DocumentPackage onHandleCallback(String json) {
        Validate.notNull(json)
        println json
        //--- deserialize json
        Event event = deserializeEvent(json)
        EventEnum eventEnum = null;

        try {
            eventEnum = EventEnum.valueOf(event.name)
        } catch (Exception ex) {
        }

        switch (eventEnum) {
            case EventEnum.PACKAGE_COMPLETE:
                tspPackageCompleteCb.setPackageId(event.getPackageId())
                tspPackageCompleteCb.handle()
                break
            case EventEnum.PACKAGE_ATTACHMENT:
                if (tspSendEmailToPackageOwnerOnPackageAttach) {
                    sendEmailOnEventCb.setPackageId(event.getPackageId())
                    ((SendEmailOnEvent) sendEmailOnEventCb).fromEmail = fromEmail
                    ((SendEmailOnEvent) sendEmailOnEventCb).subject = packageAttachEmailSubject
                    ((SendEmailOnEvent) sendEmailOnEventCb).body = packageAttachEmailBody
                    sendEmailOnEventCb.handle()
                }
                break
            default:
                break
        }
    }
}
