package gov.dss.esl.sdk.callbacks.tsp

import com.google.inject.Inject
import com.silanis.esl.sdk.*
import com.silanis.esl.sdk.io.Files
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.callbacks.BaseCallback
import gov.dss.sdk.service.MessageServiceResponse
import gov.dss.sdk.service.TspIntegrationService
import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax

import static gov.dss.sdk.service.MessageService.*

/**
 *
 * DSSCR-251: Route TSP Document. Upon the execution(or after the signing ceremony is complete) of a package we need to
 * determine where that package needs to be routed.
 *
 *
 * Author : Carvel
 * Date   : 2/10/16
 */
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class TspPackageComplete extends BaseCallback {

    @InjectConfig(value = "tspAgreementFilename")
    String tpiAgreementFilename

    @InjectConfig(value = "tspSpreadSheetFilename")
    String tpiSpreadSheetFilename

    @InjectConfig(value = "tspEmailSender")
    String fromEmail

    @InjectConfig(value = "tspEmailSubject")
    String subject

    @InjectConfig(value = "tspEmailBody")
    String body

    @InjectConfig(value = "tspSendEmailToPackageOwnerOnPackageComplete")
    String sendEmailToPackageOwnerOnPackageComplete


    @Inject
    TspPackageComplete(DssEslClient dssEslClient) {
        super(dssEslClient)
    }

    @Override
    DocumentPackage handle() {
        /**
         * get package from eSignLive
         *
         * This should be a reusable chunk of code
         */
        DocumentPackage documentPackage = dssEslClient.getPackage(new PackageId(packageId))
        Document document = documentPackage.documents[1]
        println "download document ${document.name} from eSignLive"
        byte[] agreementBytes = dssEslClient.downloadDocument(documentPackage.id, document.id.toString())
        File tpiAgreement = new File(tpiAgreementFilename as String)
        Files.saveTo(agreementBytes, tpiAgreement)

        /**
         * extract form fields from pdf
         */
        MessageServiceResponse parsedPdf = dssEslClient.tspIntegrationService.sendMessage([(ACTION): (PARSE), (FILE): tpiAgreement])
        TspIntegrationService.TPIData tpiData = parsedPdf.payload as TspIntegrationService.TPIData

        /**
         * validate data
         */
        MessageServiceResponse validatedTpiData = dssEslClient.tspIntegrationService.sendMessage([(ACTION): (VALIDATE), (TPI_DATA): tpiData])

        /**
         * generate excel file
         */
        dssEslClient.tspIntegrationService.sendMessage([(ACTION): (GENERATE_EXCEL_FILE), (TPI_DATA): validatedTpiData.payload])

        /**
         * email spread sheet to package owner
         */
        SenderInfo senderInfo = getPackageOwner(documentPackage)

        if (Boolean.parseBoolean(sendEmailToPackageOwnerOnPackageComplete)) {
            sendEmailToPackageOwner(senderInfo.email)
        }

        documentPackage
    }

    SenderInfo getPackageOwner(DocumentPackage documentPackage) {
        SenderInfo senderInfo
        for (Signer signer : documentPackage.getSigners()) {
            senderInfo = documentPackage.getSenderInfo()
            if (senderInfo.email.equalsIgnoreCase(signer.email)) {
                break
            }
        }
        senderInfo
    }

    private void sendEmailToPackageOwner(String email) {
        dssEslClient.gmailService.sendMessage([(RECEIVER): email, (FROM): fromEmail, (SUBJECT): subject, (BODY): body, (ATTACHMENT_FILE_NAME): tpiSpreadSheetFilename])
    }
}
