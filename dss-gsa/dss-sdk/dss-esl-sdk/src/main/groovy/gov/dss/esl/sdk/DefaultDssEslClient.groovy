package gov.dss.esl.sdk

import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.RandomAccessFileOrArray
import com.itextpdf.text.pdf.parser.PdfTextExtractor
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy
import com.silanis.esl.api.util.EmailValidator
import com.silanis.esl.sdk.*
import com.silanis.esl.sdk.builder.*
import com.silanis.esl.sdk.internal.EslServerException
import com.silanis.esl.sdk.service.*
import gov.dss.esl.sdk.utilities.service.ExceptionHandlerService
import gov.dss.esl.sdk.utilities.service.ResponseBuilder
import gov.dss.sdk.service.*
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.StringUtils
import org.apache.poi.openxml4j.exceptions.InvalidOperationException
import org.apache.poi.openxml4j.opc.OPCPackage
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import sun.misc.BASE64Decoder

import java.nio.file.Path

import static com.silanis.esl.sdk.builder.DocumentPackageAttributesBuilder.newDocumentPackageAttributes
import static com.silanis.esl.sdk.builder.PackageBuilder.newPackageNamed
import static com.silanis.esl.sdk.builder.SignatureBuilder.*
import static com.silanis.esl.sdk.builder.SignerBuilder.newSignerWithEmail
import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC
import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING

/**
 *
 * Default implementation of the DssEslClient interface that simply delegates its operations to the
 * com.silanis.sdk.EslClient implementation. This layer of abstraction enables us to
 * exercise  our code base without the requirement of a live  API endpoint.
 *
 * There are additional methods added specifically for DSS requirements
 *
 * Created by Carvel on 11/21/15 @ 2:41 PM.
 *
 */
class DefaultDssEslClient implements DssEslClient {
    AlfrescoService alfrescoService

    GmailService gmailService

    GoogleDriveService2LO googleDriveService

    LocalFileService localFileService

    TspIntegrationService tspIntegrationService

    final ERROR_MESSAGE_PREFIX = "Error"

    /**
     * The EslClient delegate
     */
    EslClient delegate

    /**
     * Gets the package service
     * @return the package service
     */

    @Override
    PackageService getPackageService() {
        delegate.packageService
    }

    MessageService getTspIntegrationService() {
        tspIntegrationService
    }

    /**
     * Gets the report service
     * @return the report service
     */
    @Override
    ReportService getReportService() {
        delegate.reportService
    }

    @Override
    SessionService getSessionService() {
        delegate.sessionService
    }

    /**
     * Facilitates access to the service that provides a summary of all the document fields and their values
     *
     * @return the field summary service
     */
    @Override
    FieldSummaryService getFieldSummaryService() {
        delegate.fieldSummaryService
    }

    @Override
    AuditService getAuditService() {
        delegate.auditService
    }

    /**
     * Facilitates access to the service that could be used to register for event notifications
     *
     * @return the event notification service
     */
    @Override
    EventNotificationService getEventNotificationService() {
        delegate.eventNotificationService
    }

    /**
     * Facilitates access to the service that creates authentication tokens
     *
     * @return the authentication token service
     */
    @Override
    AuthenticationTokensService getAuthenticationTokensService() {
        delegate.authenticationTokensService
    }

    /**
     * Facilitates access to the service that could be used to add custom field
     *
     * @return the custom field service
     */
    @Override
    CustomFieldService getCustomFieldService() {
        delegate.customFieldService
    }

    /**
     * <p>Creates the package.</p>
     * <p>This basically does the followings:</p>
     * <p> - converts the document package to JSON format</p>
     * <p> - makes an eSL REST call to actually create the package. Is is using as payload the above generated JSON content.
     *
     * @param documentPackage the document package
     * @return the package ID
     */
    @Override
    PackageId createPackage(DocumentPackage documentPackage) {
        delegate.createPackage(documentPackage)
    }

    /**
     * <p>Update the package, when the package status is only Draft</p>
     *
     * @param packageId
     * @param documentPackage the document package
     */
    @Override
    void updatePackage(PackageId packageId, DocumentPackage documentPackage) {
        delegate.updatePackage(packageId, documentPackage)
    }

    /**
     * <p>Change the package's status from SENT to DRAFT.</p>
     *
     * @param packageId
     */
    @Override
    void changePackageStatusToDraft(PackageId packageId) {
        delegate.changePackageStatusToDraft(packageId)
    }

    /**
     * Creates the package in one step
     *
     * WARNING: DOES NOT WORK WHEN SENDER HAS A SIGNATURE
     *
     * @param documentPackage the document package
     * @return the package ID
     */
    @Override
    PackageId createPackageOneStep(DocumentPackage documentPackage) {
        delegate.createPackageOneStep(documentPackage)
    }

    /**
     * <p>Creates a new packages, and immediately sends it to be signed.</p>
     *
     * @param documentPackage the document package to be created and signed
     * @return the packageId for the newly created package.
     */
    @Override
    PackageId createAndSendPackage(DocumentPackage documentPackage) {
        delegate.createAndSendPackage(documentPackage)
    }

    /**
     * Creates a package based on an existent template
     *
     * @param packageId the package ID used as template for the new package
     * @param documentPackage the document package
     * @return the package ID
     */
    @Override
    PackageId createPackageFromTemplate(PackageId packageId, DocumentPackage documentPackage) {
        delegate.createPackageFromTemplate(packageId, documentPackage)
    }

    /**
     * <p>It does the followings for the package ID specified as argument:</p>
     * <p> - activates the package</p>
     * <p> - send emails to signers and the package owner</p>
     * <p> - sends notifications (if any)</p>
     *
     * @param id the package ID
     */
    @Override
    void sendPackage(PackageId id) {
        delegate.sendPackage(id)
    }

    /**
     * Retrieves a summary for all the document fields of the package identified by its packageId
     * @param packageId the package ID
     * @return a list of field summarys
     */
    @Override
    List<FieldSummary> getFieldValues(PackageId packageId) {
        delegate.getFieldValues(packageId)
    }

    /**
     * @param packageId The document package identifier
     * @return the document package with the given packageId
     */
    @Override
    DocumentPackage getPackage(PackageId packageId) {
        delegate.getPackage(packageId)
    }

    /**
     * Downloads a document that belongs to a package
     * @param packageId the package ID
     * @param documentId the document ID
     * @return the content of the document
     */
    @Override
    byte[] downloadDocument(PackageId packageId, String documentId) {
        delegate.downloadDocument(packageId, documentId)
    }

    /**
     * Downloads an original document that belongs to a package.
     * @param packageId the package ID
     * @param documentId the document ID
     * @return the content of the original document
     */
    @Override
    byte[] downloadOriginalDocument(PackageId packageId, String documentId) {
        delegate.downloadDocument(packageId, documentId)
    }

    /**
     * Downloads the evidence summary for a package
     * @param packageId the package ID
     * @return the content of the evidence summary
     */
    @Override
    byte[] downloadEvidenceSummary(PackageId packageId) {
        delegate.downloadEvidenceSummary(packageId)
    }

    /**
     * Downloads the zipped documents of a package
     * @param packageId the package ID
     * @return the zipped documents
     */
    @Override
    byte[] downloadZippedDocuments(PackageId packageId) {
        delegate.downloadZippedDocuments(packageId)
    }

    @Override
    SigningStatus getSigningStatus(PackageId packageId, SignerId signerId, DocumentId documentId) {
        delegate.getSigningStatus(packageId, signerId, documentId)
    }

    @Override
    Document uploadDocument(String fileName, byte[] fileContent, Document document, DocumentPackage documentPackage) {
        delegate.uploadDocument(fileName, fileContent, document, documentPackage)
    }

    @Override
    Document uploadDocument(Document document, DocumentPackage documentPackage) {
        delegate.uploadDocument(document, documentPackage)
    }

    @Override
    void uploadAttachment(PackageId packageId, String attachmentId, String filename, byte[] fileContent, String signerId) {
        delegate(packageId, attachmentId, filename, signerId)
    }

    @Override
    GroupService getGroupService() {
        delegate.groupService
    }

    @Override
    AccountService getAccountService() {
        delegate.accountService
    }

    @Override
    ApprovalService getApprovalService() {
        delegate.approvalService
    }

    @Override
    ReminderService getReminderService() {
        delegate.reminderService
    }

    @Override
    TemplateService getTemplateService() {
        delegate.templateService
    }

    @Override
    AttachmentRequirementService getAttachmentRequirementService() {
        delegate.attachmentRequirementService
    }

    @Override
    LayoutService getLayoutService() {
        delegate.layoutService
    }

    @Override
    QRCodeService getQrCodeService() {
        delegate.qrCodeService
    }

    @Override
    SystemService getSystemService() {
        delegate.systemService
    }
    /**
     *
     * @return
     */
    MessageService getLocalFileService() {
        localFileService
    }

    MessageService getAlfrescoService() {
        alfrescoService
    }

    @Override
    MessageService getGmailService() {
        gmailService
    }

    /**
     *
     * @return
     */
    @Override
    MessageService getGoogleDriveService() {
        googleDriveService
    }

    /**
     *
     * DSSCR-125 Bulk Identities
     *
     * @param json
     * @return
     */
    @Override
    public String createIdentities(File json) {
        createIdentities(json.text)
    }

    private List<AccountMember> createAccountMembers(identities) {
        List<AccountMember> accountMembers = new ArrayList<>()
        identities.identities.each {
            AccountMember accountMember = new AccountMember()
            accountMember.email = it.email
            accountMember.phoneNumber = it.phoneNumber
            accountMember.title = it.title
            accountMember.company = ""
            accountMember.firstName = ""
            accountMember.lastName = ""
            accountMembers.add(accountMember)
        }
        accountMembers
    }

    private String inviteUser(AccountMember accountMember) {
        String success = "Success inviting : %s"
        String error = "${ERROR_MESSAGE_PREFIX} inviting %s : %s"
        String message
        try {
            Sender invite = delegate.accountService.inviteUser(accountMember)
            message = String.format(success, invite.email)
        } catch (EslServerException ex) {
            message = String.format(error, accountMember.email, ex.serverError.message)
        }
        message
    }

    public String[] createIdentities(String json) {
        def messages = []
        def identities = new JsonSlurper().parseText(json)
        def accountMembers = createAccountMembers(identities)
        accountMembers.each {
            messages.add(inviteUser(it as AccountMember))
        }
        messages
    }

    private File writeExcelFileToLocalDisk(InputStream inputStream) {
        //--- We are assuming that the file extension will be xlsx because we have no way
        //--- to determine the excel file type based on an InputStream
        String ext = "xlsx"
        String name = String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), ext);
        String fullyQualifiedFiledName = System.getProperty("java.io.tmpdir") + File.separator + name

        OutputStream outputStream = null

        try {
            // write the inputStream to a FileOutputStream
            outputStream = new FileOutputStream(new File(fullyQualifiedFiledName))

            int read = 0
            byte[] bytes = new byte[1024]

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read)
            }
        } catch (IOException e) {
            e.printStackTrace()
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (IOException e) {
                    e.printStackTrace()
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush()
                    outputStream.close()
                } catch (IOException e) {
                    e.printStackTrace()
                }

            }
        }
        new File(fullyQualifiedFiledName)
    }


    private File writePDFFileToLocalDisk(InputStream inputStream) {
        String ext = "pdf"
        String name = String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), ext);
        String fullyQualifiedFiledName = System.getProperty("java.io.tmpdir") + File.separator + name

        OutputStream outputStream = null

        try {
            // write the inputStream to a FileOutputStream
            outputStream = new FileOutputStream(new File(fullyQualifiedFiledName))

            int read = 0
            byte[] bytes = new byte[1024]

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read)
            }
        } catch (IOException e) {
            e.printStackTrace()
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (IOException e) {
                    e.printStackTrace()
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush()
                    outputStream.close()
                } catch (IOException e) {
                    e.printStackTrace()
                }

            }
        }
        new File(fullyQualifiedFiledName)
    }

    @Override
    String[] createIdentities(InputStream inputStream) {
        def messages = []
        def header = []
        def values = []
        OPCPackage pkg

        File tmpExcelFile = writeExcelFileToLocalDisk(inputStream)

        try {

            pkg = OPCPackage.open(tmpExcelFile);
            def workbook = new XSSFWorkbook(pkg)

            def sheet = workbook.getSheetAt(0)

            for (cell in sheet.getRow(0).cellIterator()) {
                header << cell.stringCellValue
            }

            def headerFlag = true
            for (row in sheet.rowIterator()) {
                if (headerFlag) {
                    headerFlag = false
                    continue
                }

                def rowData = [:]
                for (cell in row.cellIterator()) {
                    def value = ''

                    switch (cell.cellType) {
                        case CELL_TYPE_STRING:
                            value = cell.stringCellValue
                            break
                        case CELL_TYPE_NUMERIC:
                            value = cell.numericCellValue as Integer
                            break
                        default:
                            value = ''
                    }
                    rowData << ["${header[cell.columnIndex]}": value]
                }
                values << rowData
            }
            messages = createIdentities("{\"identities\":" + JsonOutput.toJson(values) + "}")
        } catch (InvalidOperationException ex) {
            messages.add("${ERROR_MESSAGE_PREFIX} : Please make sure that you upload a valid Excel file containing the following column headers[email(required),title,phoneNumber]")
        }
        finally {
            if (pkg != null)
                pkg.close();
            FileUtils.forceDelete(tmpExcelFile)
        }


        return messages
    }

    /**
     * DSSCR-132 Signature Block Insertion
     */
    @Override
    String[] insertSignatureBlock(InputStream inputStream,
                                  String documentName,
                                  String ownerEmail,
                                  String newPackageName,
                                  List<String> listSignerEmails,
                                  List<String> listTexts,
                                  List<String> listSignTypes,
                                  List<String> listFirstName,
                                  List<String> listLastName,
                                  String assignOrder,
                                  String createSend) {
        def messages = []
/**
 * sprint9 code review
 * Sudganhi Todo: externalize the field width & height hardcoding
 *
 */
        int FIELD_WIDTH = 145
        int FIELD_HEIGHT = 45
        int occurrences1 = 0;
        String filePath = null;
        PdfReader pdfReader = null;
        String type = null;
        EmailValidator validator = new EmailValidator();
        SignatureBuilder signBuild;
        String docName = null;
        Path p = null;

        PackageBuilder package1 = newPackageNamed(newPackageName);
        File tmpPDFFile = writePDFFileToLocalDisk(inputStream)
        filePath = tmpPDFFile.getCanonicalPath()

        docName = documentName.substring(12);
        Document mydoc = DocumentBuilder.newDocumentWithName(docName)
                .fromFile(filePath)
                .build();

        if (validator.valid(ownerEmail) == false) {
            messages.add("${ERROR_MESSAGE_PREFIX} : Invalid format of package owner email address. Please enter a " +
                    "valid email-id. ")
            return messages
        }
        package1.withSenderInfo(SenderInfoBuilder.newSenderInfo(ownerEmail));

        for (int i = 0; i < listSignerEmails.size(); i++) {
            System.out.println(i);
            type = listSignTypes[i];
            occurrences1 = 0;

            if (listFirstName[i] == "NullException") {
                messages.add("${ERROR_MESSAGE_PREFIX} : Signer's first name is mandatory.")
            }

            if (listLastName[i] == "NullException") {
                messages.add("${ERROR_MESSAGE_PREFIX} : Signer's last name is mandatory.")
            }

            switch (type) {
                case "initialsFor":
                    signBuild = initialsFor(listSignerEmails[i]);
                    break;
                case "signatureFor":
                    signBuild = signatureFor(listSignerEmails[i]);
                    break;
                case "captureFor":
                    signBuild = captureFor(listSignerEmails[i]);
                    break;
                case "mobileCaptureFor":
                    signBuild = mobileCaptureFor(listSignerEmails[i]);
                    break;

            }

            if (validator.valid(listSignerEmails[i]) == false) {
                messages.add("${ERROR_MESSAGE_PREFIX} : Invalid signer email address. Please enter a valid email-id. ")
            }

            try {

                pdfReader = new PdfReader(new RandomAccessFileOrArray(filePath), null);

                for (int page = 1; page <= pdfReader.getNumberOfPages(); page++) {
                    String currentPageText = null;
                    try {
                        SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        currentPageText = PdfTextExtractor.getTextFromPage(pdfReader, page, strategy);
                    } catch (InvalidOperationException e) {
                        e.printStackTrace()
                    }
                    while (currentPageText.contains(listTexts[i])) {
                        occurrences1++;
                        currentPageText = currentPageText.substring(currentPageText.indexOf(listTexts[i]) + listTexts[i]
                                .length());
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                messages.add("${ERROR_MESSAGE_PREFIX} : Please make sure that you upload a valid PDF file.");
            }
            if (occurrences1 > 0) {

                TextAnchorBuilder textAnchorBuilder = TextAnchorBuilder.newTextAnchor(listTexts[i]);
                for (int j = 0; j < occurrences1; j++) {
                    mydoc.getSignatures().add(signBuild
                            .withPositionAnchor(textAnchorBuilder
                            .atPosition(TextAnchorPosition.BOTTOMRIGHT)
                            .withSize(FIELD_WIDTH, FIELD_HEIGHT)
                            .withOffset(0, 0)
                            .withCharacter(2)
                            .withOccurence(j)
                    ).build());

                }
                mydoc.setIndex(1)
                if (assignOrder == "assignOrder") {
                    package1.withSigner(newSignerWithEmail(listSignerEmails[i])
                            .withRoleId("Signer" + i)
                            .withFirstName(listFirstName[i])
                            .withLastName(listLastName[i])
                            .signingOrder(i));
                } else {
                    package1.withSigner(newSignerWithEmail(listSignerEmails[i])
                            .withRoleId("Signer" + i)
                            .withFirstName(listFirstName[i])
                            .withLastName(listLastName[i]));
                }

            } else {
                messages.add("${ERROR_MESSAGE_PREFIX} : Search text does not exists in the document. Please enter a valid" +

                        " text.")
            }
        }
        package1.withDocument(mydoc);
        tmpPDFFile.delete();
        try {
            DocumentPackage completePackage = package1.build();
            PackageId packageId = delegate.createPackage(completePackage);
            if (createSend == "createSend") {
                delegate.sendPackage(packageId)
                messages.add("Success: Package created and sent.")

            } else {
                messages.add("Success: Package created and available in the draft folder.")
            }
        }
        catch (Exception e) {
            if (e instanceof EslServerException) {
                messages.add("${ERROR_MESSAGE_PREFIX} : This Package Owner does not have an eSignLive account.")
            } else if (e instanceof EslException) {
                messages.add("${ERROR_MESSAGE_PREFIX} : This Package could not be created.")
            } else {
                messages.add("${ERROR_MESSAGE_PREFIX} : Unknown problem.")
            }
            e.printStackTrace();
            return messages
        }
        return messages
    }

    /**
     *
     * Validate the dssUniversalConnector data
     * @param signatureInsertionData
     */

    def validateData(Map<String, Object> signatureInsertionData) {
        def messageList = [];
        EmailValidator validator = new EmailValidator();
        String fileName;
        int nameLength;
        String extension;
        ExceptionHandlerService exceptionHandlerService = new ExceptionHandlerService();
        def messageMap;

        if ((signatureInsertionData.containsKey(signatureInsertionData.orgName))||(StringUtils.isEmpty(signatureInsertionData.orgName))) {
            messageMap = exceptionHandlerService.parseValidationErrors("Organization Name not provided or is empty.",
                                                                        550,
                                                                       "Validation Error.");
        }
        if ((signatureInsertionData.containsKey(signatureInsertionData.packageName))||(StringUtils.isEmpty(signatureInsertionData.packageName))) {
            messageMap = exceptionHandlerService.parseValidationErrors("Package Name not provided or is empty.",
                                                                        540,
                                                                        "Validation Error.");
        }

            if ((signatureInsertionData.containsKey(signatureInsertionData.senderEmail))||(StringUtils.isEmpty(signatureInsertionData.senderEmail))) {
                messageMap = exceptionHandlerService.parseValidationErrors("Sender email not provided or is empty.",
                                                                            541,
                                                                            "Validation Error.");
            } else {
                if (validator.valid(signatureInsertionData.senderEmail) == false) {
                    messageMap = exceptionHandlerService.parseValidationErrors("Sender email address not provided or is invalid.",
                            542,
                            "Validation Error.");
                }
            }


            /*This is a Boolean type variable so will need to check for null value and not 'if String empty'.
            If the user does not provide a value for 'enableSigningOder' then it will be assigned a default value of
            'false'*/
            if ((signatureInsertionData.containsKey(signatureInsertionData.enableSigningOrder))||(signatureInsertionData.enableSigningOrder == null)) {
                signatureInsertionData.enableSigningOrder = false;
            }

            if ((signatureInsertionData.containsKey(signatureInsertionData.packageOption))||(StringUtils.isEmpty(signatureInsertionData.packageOption))) {
                messageMap = exceptionHandlerService.parseValidationErrors("Package Option not provided or is empty. Valid values are ‘Create’ or ‘CreateSend’.",
                        543,
                        "Validation Error.");
            }

            def documentsMap = signatureInsertionData.documents;
            for (int i = 0; i < documentsMap.size(); i++) {
                if ((signatureInsertionData.containsKey(documentsMap[i].getAt("document").getAt("documentName")))||(StringUtils.isEmpty(documentsMap[i].getAt("document").getAt("documentName")))) {
                    messageMap = exceptionHandlerService.parseValidationErrors("Document Name not provided or is empty.",
                            544,
                            "Validation Error.");
                } else {
                    /*Make sure that the user gives a filename with PDF extension only*/
                    fileName = documentsMap[i].getAt("document").getAt("documentName");
                    nameLength = fileName.length();
                    extension = fileName.substring((nameLength - 3), nameLength);
                    if ((!(extension.equalsIgnoreCase("pdf"))) || (nameLength == 3)) {
                        messageMap = exceptionHandlerService.parseValidationErrors("Document Name is invalid. The API accepts .pdf documents only.",
                                562,
                                "Validation Error.");
                    }
                }

                if ((signatureInsertionData.containsKey(documentsMap[i].getAt("document").getAt("documentContent")))||(StringUtils.isEmpty(documentsMap[i].getAt("document").getAt("documentContent")))) {
                    /*TODO Verify that the file is of type PDF*/
                    messageMap = exceptionHandlerService.parseValidationErrors("Document Content not provide or is empty. The API accepts Document Content as base64Encoded String.",
                            545,
                            "Validation Error.");

                }
                def signersArray = documentsMap[i].getAt("document").getAt("signers");
                for (int j = 0; j < signersArray.size(); j++) {

                    if ((signatureInsertionData.containsKey(signersArray[j].getAt("signerEmail")))||(StringUtils.isEmpty(signersArray[j].getAt("signerEmail")))) {
                        messageMap = exceptionHandlerService.parseValidationErrors("Signer email not provided or is " +
                                "empty.",
                                546,
                                "Validation Error.");
                    } else {
                        if (validator.valid(signersArray[j].getAt("signerEmail")) == false) {
                            messageMap = exceptionHandlerService.parseValidationErrors("Signer email not provided or is " +
                                    "invalid.",
                                    547,
                                    "Validation Error.");
                        }
                    }


                    if ((signatureInsertionData.containsKey(signersArray[j].getAt("signerFirstName")))||(StringUtils.isEmpty(signersArray[j].getAt("signerFirstName")))) {
                        messageMap = exceptionHandlerService.parseValidationErrors("Signer First Name not provided or" +
                                " is empty.",
                                548,
                                "Validation Error.");
                    }

                    if ((signatureInsertionData.containsKey(signersArray[j].getAt("signerLastName")))||(StringUtils.isEmpty(signersArray[j].getAt("signerLastName")))) {
                        messageMap = exceptionHandlerService.parseValidationErrors("Signer Last Name not provided or " +
                                "is empty.",
                                549,
                                "Validation Error.");
                    }
                    /*If signature position is not given by user then default it to TOP_RIGHT.*/
                    if ((signatureInsertionData.containsKey(signersArray[j].getAt("signaturePosition")))||(StringUtils.isEmpty(signersArray[j].getAt("signaturePosition")))) {
                        signersArray[j].putAt("signaturePosition", "TOPRIGHT");
                    }

                    /*If the user gives a packageOption of createSend but does not give a search text then send a validation error message*/
                    if ((signatureInsertionData.packageOption == "createSend") && (StringUtils.isEmpty(signersArray[j].getAt("searchText")))) {
                        messageMap = exceptionHandlerService.parseValidationErrors("Search text not provided or " +
                                "is empty.",
                                561,
                                "Validation Error.");
                    }

                    /*If the user does not provide a value for 'signType' then it will be assigned a default value of 'signatureFor'*/
                    if ((signatureInsertionData.containsKey(signersArray[j].getAt("signType")))||(StringUtils.isEmpty(signersArray[j].getAt("signType")))) {
                        signersArray[j].putAt("signType", "signatureFor");
                    }
                }
            }
        return messageMap;
    }

    /**
     *
     *
     * @param encodedString
     */
    InputStream decodeBase64String(String encodedString) {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes = decoder.decodeBuffer(encodedString);
        InputStream bufferedInputStream = new ByteArrayInputStream(decodedBytes);
        return bufferedInputStream;
    }

    /**
     *
     * This method is used to convert signationPosition string to TextAnchorPosition enum.
     * @param signPosition
     */
    TextAnchorPosition textAnchorPosition(String signPosition) {
        TextAnchorPosition myanchor;
        switch (signPosition.toUpperCase()) {
            case "BOTTOMRIGHT":
                myanchor = TextAnchorPosition.BOTTOMRIGHT;
                break;
            case "BOTTOMLEFT":
                myanchor = TextAnchorPosition.BOTTOMLEFT;
                break;
            case "TOPRIGHT":
                myanchor = TextAnchorPosition.TOPRIGHT;
                break;
            case "TOPLEFT":
                myanchor = TextAnchorPosition.TOPLEFT;
                break;
        }
        return myanchor;
    }

    /**
     *
     *
     * @param signatureInsertionData
     */
    Map<String, Object> dssUniversalConnector(Map<String, Object> signatureInsertionData) {
        int FIELD_WIDTH = 145
        int FIELD_HEIGHT = 45
        int occurrences1 = 0;
        String filePath = null;
        PdfReader pdfReader = null;
        String type = null;
        SignatureBuilder signBuild;
        String fileName = null;
        SignerBuilder signer;
        String docName = null;
        Path p = null;
        def messageList = [];
        Map<String, String> messageMap = new HashMap<String, String>();
        ExceptionHandlerService exceptionHandlerService = new ExceptionHandlerService();
        ResponseBuilder responseBuilder = new ResponseBuilder();
        String successMessage;

        /*Validate the Map signatureInsertionData that comes in.*/
        messageMap = validateData(signatureInsertionData);

        if (!(messageMap==null)) {
            return messageMap
        }

        /*Create the Package.*/
        PackageBuilder package1 = newPackageNamed(signatureInsertionData.packageName);
        package1.withSenderInfo(SenderInfoBuilder.newSenderInfo(signatureInsertionData.senderEmail));

        def documentsMap = signatureInsertionData.documents;
        for (int i = 0; i < documentsMap.size(); i++) {
            docName = (documentsMap[i].getAt("document").getAt("documentName"));

            /*Convert base64 encoded file String into InputStream*/
            InputStream bufferedInputStream = null;
            try {
                bufferedInputStream = decodeBase64String(documentsMap[i].getAt("document").getAt("documentContent"));
            }
            catch (Exception e) {
                /*This is when the base64 encoded file is corrupt and ends up with an exception while decoding it*/
                messageMap = exceptionHandlerService.parseValidationErrors("File could not be decoded.",
                        564,
                        "Validation Error");
                return messageMap
            }
            File tmpPDFFile = writePDFFileToLocalDisk(bufferedInputStream)
            filePath = tmpPDFFile.getCanonicalPath()
            bufferedInputStream.close()

            Document mydoc = DocumentBuilder.newDocumentWithName(docName)
                    .fromFile(filePath)
                    .build();
            def signersArray = documentsMap[i].getAt("document").getAt("signers");
            for (int j = 0; j < signersArray.size(); j++) {
                type = signersArray[j].getAt("signType");
                occurrences1 = 0;

                switch (type) {
                    case "ClickToInitial":
                        signBuild = initialsFor(signersArray[j].getAt("signerEmail"));
                        break;
                    case "ClickToSign":
                        signBuild = signatureFor(signersArray[j].getAt("signerEmail"));
                        break;
                    case "CaptureSignature":
                        signBuild = captureFor(signersArray[j].getAt("signerEmail"));
                        break;
                    case "MobileCapture":
                        signBuild = mobileCaptureFor(signersArray[j].getAt("signerEmail"));
                        break;
                }

                if (StringUtils.isNotEmpty(signersArray[j].getAt("searchText"))) {
                    pdfReader = new PdfReader(new RandomAccessFileOrArray(filePath), null);

                    for (int page = 1; page <= pdfReader.getNumberOfPages(); page++) {
                        String currentPageText = null;
                        try {
                            SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                            currentPageText = PdfTextExtractor.getTextFromPage(pdfReader, page, strategy);
                        } catch (InvalidOperationException e) {
                            e.printStackTrace()
                        }
                        while (currentPageText.contains(signersArray[j].getAt("searchText"))) {
                            occurrences1++;
                            currentPageText = currentPageText.substring(currentPageText.indexOf(signersArray[j].getAt
                                    ("searchText")) + signersArray[j].getAt("searchText").length());
                        }
                    }
                    if (occurrences1 == 0) {
                        /*If the search text does not exists in the document then an error message shoudl be displayed.
                        The package should only be created in draft mode with the signer info.*/
//                        signatureInsertionData.packageOption = "create";
                        messageMap = exceptionHandlerService.parseValidationErrors("Search Text not found in the document.",
                                                                                    563,
                                                                                    "Validation Error");
                        return messageMap

                    }
                    pdfReader.close();
                }
                if (occurrences1 > 0) {
                    TextAnchorBuilder textAnchorBuilder = TextAnchorBuilder.newTextAnchor(signersArray[j].getAt
                            ("searchText"));
                    for (int k = 0; k < occurrences1; k++) {
                        mydoc.getSignatures().add(signBuild
                                .withPositionAnchor(textAnchorBuilder
                                .atPosition(textAnchorPosition(signersArray[j].getAt("signaturePosition")))
                                .withSize(FIELD_WIDTH, FIELD_HEIGHT)
                                .withOffset(0, 0)
                                .withCharacter(2)
                                .withOccurence(k)
                        ).build());

                    }
                    mydoc.setIndex(1)
                }
                if (signatureInsertionData.enableSigningOrder == true) {
                    signer = SignerBuilder.newSignerWithEmail(signersArray[j].getAt("signerEmail"))
                            .withFirstName(signersArray[j].getAt("signerFirstName"))
                            .withLastName(signersArray[j].getAt("signerLastName"))
                            .signingOrder(j);
                } else {
                    signer = SignerBuilder.newSignerWithEmail(signersArray[j].getAt("signerEmail"))
                            .withFirstName(signersArray[j].getAt("signerFirstName"))
                            .withLastName(signersArray[j].getAt("signerLastName"));
                }
                package1.withSigner(signer);
            }
            package1.withDocument(mydoc);
            tmpPDFFile.delete();
        }
        try {
            /*The package attribute is set to the organization name using withAttributes() method*/
            DocumentPackage completePackage = package1
                    .withAttributes(newDocumentPackageAttributes()
                    .withAttribute("orgName", signatureInsertionData.orgName)
                    .build())
                    .build();

            PackageId packageId = delegate.createPackage(completePackage);

            if (signatureInsertionData.packageOption == "createSend") {
                delegate.sendPackage(packageId)
                successMessage = "Package created and sent."
            } else {
                successMessage = "Package created and available in the draft folder."
            }
            messageMap = responseBuilder.buildSuccessResponse(successMessage, packageId.toString(), completePackage.getName())
        }
        catch (Exception e) {
            messageMap = exceptionHandlerService.parseException(e);
        }
        return messageMap
    }

    SenderStatus mapStatus(String status) {
        SenderStatus _status = SenderStatus.INVITED
        if (StringUtils.isBlank(status))
            return _status

        if (status.equalsIgnoreCase(SenderStatus.ACTIVE.name()))
            _status = SenderStatus.ACTIVE
        else if (status.equalsIgnoreCase(SenderStatus.LOCKED.name()))
            _status = SenderStatus.LOCKED
        _status
    }

    EslClient getDelegate() {
        return delegate
    }

    void setDelegate(EslClient delegate) {
        this.delegate = delegate
    }

}
