package gov.dss.esl.sdk.callbacks

import gov.dss.esl.sdk.DssEslClient

/**
 * Abstraction for CallbackHandlers
 *
 * Author : Carvel 
 * Date   : 3/24/16
 */
interface CallbackHandler {
    DssEslClient getDssEslClient()
    void handleCallback(String json)
}