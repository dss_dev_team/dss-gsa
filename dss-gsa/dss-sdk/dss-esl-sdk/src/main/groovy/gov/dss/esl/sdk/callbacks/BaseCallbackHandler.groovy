package gov.dss.esl.sdk.callbacks

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.inject.Inject
import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.io.Files
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.annontations.DefaultDssClientAnnotation
import gov.dss.sdk.service.GoogleDriveService2LO

import static gov.dss.sdk.service.MessageService.*

/**
 * Base class for Callbackhander implementations.
 *
 * Author : Carvel
 * Date   : 4/28/16
 */
abstract class BaseCallbackHandler implements CallbackHandler {
    @Inject
    @DefaultDssClientAnnotation
    DssEslClient dssEslClient

    protected Event deserializeEvent(String json) {
        Event event
        json = sanitizeAndCompress(json)
        try {
            ObjectMapper mapper = new ObjectMapper()
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            event = mapper.readValue(json, Event.class)
        } catch (Exception ex) {
            println("Could not deserialize the  event" + json)
        }
        return event
    }

    /**
     * This implementation of handleCallback() will copy all of the documents inside of a fully
     * executed eSignLive signing ceremony to a specified google drive directory on behalf of all subclasses
     *
     * @param json
     */
    void handleCallback(String json) {
        println "==> ${this.class.name} handling callback [$json]"
        DocumentPackage documentPackage = onHandleCallback(json);
        /*8/30/2016 : The implementation of Google Drive is now out of scope.*/
        /*if (documentPackage != null) {
            /**
             * copy all files to google drive
             */
            /*byte[] zip = dssEslClient.downloadZippedDocuments(documentPackage.id)
            Files.saveTo(zip, documentPackage.name + ".zip")
            dssEslClient.googleDriveService.sendMessage([(ACTION): (UPLOAD), (FILE_NAME): documentPackage.name + ".zip", (FOLDER): googleDriveFolderName])

            /**
             * copy evidence summary to google drive
             */
            /*byte[] evidenceSummary = dssEslClient.downloadEvidenceSummary(documentPackage.id)
            Files.saveTo(evidenceSummary, "evidence.pdf")
            dssEslClient.googleDriveService.sendMessage([(ACTION): (UPLOAD), (FILE_NAME): "evidence.pdf", (FOLDER): googleDriveFolderName])

        }*/
    }

    abstract DocumentPackage onHandleCallback(String json)

    abstract String getGoogleDriveFolderName()

    GoogleDriveService2LO getGoogleDriveService() {
        googleDriveService
    }

    void setGoogleDriveService(GoogleDriveService2LO googleDriveService) {
        this.googleDriveService = googleDriveService
    }

    protected String sanitizeAndCompress(String string) {
        string.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" ", "")
    }
}

