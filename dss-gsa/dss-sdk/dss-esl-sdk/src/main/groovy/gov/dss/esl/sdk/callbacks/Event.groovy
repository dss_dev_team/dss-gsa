package gov.dss.esl.sdk.callbacks

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * DSSCR-155 Notification of package ready for completion.
 *
 * Simple abstraction that encapsulates the data needed to model a
 * event.
 *
 * Created by Carvel on 1/27/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Event {
    String packageId
    String name
    String sessionUser
    String documentId

    public Event() {
        //--- required constructor for Jackson --- Do not remove
    }

    public Event(String packageId, String name, String sessionUser) {
        this.packageId = packageId
        this.name = name
        this.sessionUser = sessionUser
    }

    @Override
    public String toString() {
        return "Event{" +
                "packageId='" + packageId + '\'' +
                ", name='" + name + '\'' +
                ", sessionUser='" + sessionUser + '\'' +
                ", documentId='" + documentId + '\'' +
                '}';
    }

    String getPackageId() {
        return packageId
    }

    void setPackageId(String packageId) {
        this.packageId = packageId
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getSessionUser() {
        return sessionUser
    }

    void setSessionUser(String sessionUser) {
        this.sessionUser = sessionUser
    }

    String getDocumentId() {
        return documentId
    }

    void setDocumentId(String documentId) {
        this.documentId = documentId
    }
}
