package gov.dss.esl.sdk.callbacks.generic

import com.google.inject.Inject
import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.PackageId
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.callbacks.BaseCallback

/**
 * Created by carvelhall on 6/23/16.
 */
class GenericPackageComplete extends BaseCallback {

    @Inject
    GenericPackageComplete(DssEslClient dssEslClient) {
        super(dssEslClient)
    }

    /**
     * just download the package
     * @return
     */
    DocumentPackage handle() {
        dssEslClient.getPackage(new PackageId(packageId))
    }
}
