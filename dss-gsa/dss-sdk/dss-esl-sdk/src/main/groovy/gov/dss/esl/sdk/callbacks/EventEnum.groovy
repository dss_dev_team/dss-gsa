package gov.dss.esl.sdk.callbacks

/**
 * DSSCR-155 Notification of package ready for completion.
 *
 * Created by Carvel on 1/27/16.
 */
enum EventEnum {
    PACKAGE_ATTACHMENT,
    PACKAGE_CREATE,
    PACKAGE_COMPLETE,
    PACKAGE_DELETE,
    PACKAGE_READY_FOR_COMPLETE
}