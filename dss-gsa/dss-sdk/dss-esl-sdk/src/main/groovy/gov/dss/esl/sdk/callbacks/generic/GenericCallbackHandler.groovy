package gov.dss.esl.sdk.callbacks.generic

import com.google.inject.Inject
import com.silanis.esl.sdk.DocumentPackage
import gov.dss.esl.sdk.annontations.GenericPackageCompleteCallbackAnnontation
import gov.dss.esl.sdk.callbacks.BaseCallbackHandler
import gov.dss.esl.sdk.callbacks.Callback
import gov.dss.esl.sdk.callbacks.Event
import gov.dss.esl.sdk.callbacks.EventEnum
import net.jmob.guice.conf.core.BindConfig
import net.jmob.guice.conf.core.InjectConfig
import net.jmob.guice.conf.core.Syntax
import org.apache.commons.lang3.Validate

/**
 * Created by carvelhall on 6/23/16.
 */
@BindConfig(value = "application", syntax = Syntax.PROPERTIES)
class GenericCallbackHandler extends BaseCallbackHandler {
    @Inject
    @GenericPackageCompleteCallbackAnnontation
    Callback callback

    @InjectConfig(value = "genericGoogleDriveFolder")
    String googleDriveFolderName

    DocumentPackage onHandleCallback(String json) {
        Validate.notNull(json)
        println json

        //--- deserialize json
        Event event = deserializeEvent(json)
        EventEnum eventEnum = null;
        try {
            eventEnum = EventEnum.valueOf(event.name)
        } catch (Exception ex) {
        }

        switch (eventEnum) {
            case EventEnum.PACKAGE_COMPLETE:
                callback.setPackageId(event.getPackageId())
                callback.handle()
                break
            default:
                break
        }
    }
}

