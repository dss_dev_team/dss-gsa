package gov.dss.esl.sdk.callbacks

import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.SenderInfo
import com.silanis.esl.sdk.Signer
import gov.dss.esl.sdk.DssEslClient

/**
 * Base class for  Callback implementations
 *
 * Created by carvelhall on 6/5/16.
 */
abstract class BaseCallback implements Callback {
    DssEslClient dssEslClient
    String packageId

    BaseCallback(DssEslClient dssEslClient) {
        this.dssEslClient = dssEslClient
    }

    SenderInfo getPackageOwner(DocumentPackage documentPackage) {
        SenderInfo senderInfo
        for (Signer signer : documentPackage.getSigners()) {
            senderInfo = documentPackage.getSenderInfo()
            if (senderInfo.email.equalsIgnoreCase(signer.email)) {
                break
            }
        }
        senderInfo
    }
}
