package gov.dss.esl.sdk.callbacks

import com.google.inject.Inject
import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.PackageId
import com.silanis.esl.sdk.SenderInfo
import gov.dss.esl.sdk.DssEslClient

import static gov.dss.sdk.service.MessageService.*

/**
 *
 * A callback that simply sends an email to a package owner.
 *
 * Created by Carvel on 4/4/16 @ 6:24 PM.
 *
 */
class SendEmailOnEvent extends BaseCallback {

    String fromEmail

    String subject

    String body

    @Inject
    SendEmailOnEvent(DssEslClient dssEslClient) {
        super(dssEslClient)
    }

    @Override
    DocumentPackage handle() {
        DocumentPackage documentPackage = dssEslClient.getPackage(new PackageId(packageId))
        SenderInfo senderInfo = getPackageOwner(documentPackage)
        dssEslClient.gmailService.sendMessage([(RECEIVER): senderInfo.email, (FROM): fromEmail, (SUBJECT): subject, (BODY): body])
        documentPackage
    }

}
