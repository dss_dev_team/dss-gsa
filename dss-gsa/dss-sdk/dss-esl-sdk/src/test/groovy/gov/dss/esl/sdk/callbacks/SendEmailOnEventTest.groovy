package gov.dss.esl.sdk.callbacks

import com.silanis.esl.sdk.*
import gov.dss.esl.sdk.DssEslClient
import gov.dss.sdk.service.MessageService
import gov.dss.sdk.service.MessageServiceResponse
import org.easymock.EasyMockRunner
import org.easymock.Mock
import org.easymock.TestSubject
import org.junit.Test
import org.junit.runner.RunWith

import static org.easymock.EasyMock.*

/**
 *
 * This TestClass uses the EasyMock framework in order to exercise the SendEmailOnEvent
 *
 * Created by Carvel on 4/4/16 @ 6:28 PM.
 *
 */
@RunWith(EasyMockRunner.class)
class SendEmailOnEventTest extends GroovyTestCase {
    @Mock
    MessageService gmailServiceMock

    @Mock(fieldName = "dssEslClient")
    DssEslClient dssEslClientMock

    @TestSubject
    SendEmailOnEvent sendEmailOnEvent = new SendEmailOnEvent()

    @Test
    void testHandle() {
        //--- setup test data
        String emailReceiver = "emailReceiver"
        String fromEmail = "fromEmail"
        String subject = "subject"
        String body = "body"

        Map<String, String> message = new HashMap<>()
        message.put(MessageService.RECEIVER, emailReceiver)
        message.put(MessageService.FROM, fromEmail)
        message.put(MessageService.SUBJECT, subject)
        message.put(MessageService.BODY, body)

        Signer signer = new Signer(emailReceiver, "firstName", "lastName", null)
        List<Signer> signers = new ArrayList<>()
        signers.add(signer)
        Document doc1 = new Document()
        Document doc2 = new Document()
        doc2.setId(new DocumentId("documentId"))
        List<Document> documents = new ArrayList<>()
        documents.add(doc1)
        documents.add(doc2)
        DocumentPackage expectedPackage = new DocumentPackage("name", signers, signers, documents, false)
        SenderInfo senderInfo = new SenderInfo()
        senderInfo.email = emailReceiver
        expectedPackage.senderInfo = senderInfo

        //--- record expected behavior
        expect(dssEslClientMock.getPackage(new PackageId("packageId"))).andReturn(expectedPackage)
        expect(dssEslClientMock.getGmailService()).andReturn(gmailServiceMock)
        expect(gmailServiceMock.sendMessage(message)).andReturn(createMock(MessageServiceResponse))
        replay(dssEslClientMock, gmailServiceMock)

        //--- exercise method
        sendEmailOnEvent.packageId = "packageId"
        sendEmailOnEvent.fromEmail = fromEmail
        sendEmailOnEvent.subject = subject
        sendEmailOnEvent.body = body
        sendEmailOnEvent.handle()

        //--- verification is called by EasyMockRunner
    }
}
