package gov.dss.esl.sdk.callbacks.tsp

import com.google.inject.Inject
import gov.dss.esl.sdk.annontations.TspPackageCompleteCallbackAnnotation
import gov.dss.esl.sdk.callbacks.Callback
import gov.dss.esl.sdk.module.TestGuiceModule

import static com.google.inject.Guice.createInjector

/**
 * Author : Carvel 
 * Date   : 2/10/16
 *
 *
 */
class TSPPackageCompleteTest extends GroovyTestCase {
    @Inject
    @TspPackageCompleteCallbackAnnotation
    Callback callback

    void setUp() {
        createInjector(new TestGuiceModule(this));
    }

    void testDependencyInject() {
        assert callback != null
        assert callback.dssEslClient != null
        assert callback.dssEslClient.delegate != null
    }

    void testPackageComplete() {
        //--- set up test data
        //--- causes 500 at eSignLive callback.packageId = "899910ae-a360-4e6c-b81c-80f30d767faf"
        callback.packageId = "8d04ab23-b55d-479e-a052-034ecca7c539"

        //--- exercise method
        callback.handle()

        //--- make assertions
    }
}

