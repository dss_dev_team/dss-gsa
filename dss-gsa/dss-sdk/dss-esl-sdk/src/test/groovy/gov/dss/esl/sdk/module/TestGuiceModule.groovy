package gov.dss.esl.sdk.module

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.name.Names
import com.silanis.esl.sdk.EslClient
import gov.dss.esl.sdk.DefaultDssEslClient
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.annontations.*
import gov.dss.esl.sdk.callbacks.Callback
import gov.dss.esl.sdk.callbacks.CallbackHandler
import gov.dss.esl.sdk.callbacks.generic.GenericCallbackHandler
import gov.dss.esl.sdk.callbacks.generic.GenericPackageComplete
import gov.dss.esl.sdk.callbacks.icap.IacpCallbackHandler
import gov.dss.esl.sdk.callbacks.icap.IacpPackageComplete
import gov.dss.esl.sdk.callbacks.tsp.TspCallbackHandler
import gov.dss.esl.sdk.callbacks.tsp.TspPackageComplete
import gov.dss.sdk.annotations.*
import gov.dss.sdk.service.*
import net.jmob.guice.conf.core.ConfigurationModule
import org.springframework.core.io.ClassPathResource

import javax.inject.Named

/**
 *
 *
 *
 * Created by Carvel on 3/23/16 @ 10:31 PM.
 *
 */
class TestGuiceModule extends AbstractModule {
    private final Object instance

    public TestGuiceModule(Object instance) {
        this.instance = instance
    }

    @Override
    protected void configure() {
        install(ConfigurationModule.create());
        requestInjection(instance);
        try {
            Properties props = new Properties()
            props.load(new FileInputStream(new ClassPathResource("application.properties").getFile().getAbsolutePath()))
            Names.bindProperties(binder(), props)
        } catch (Exception e) {
            e.printStackTrace()
        }
        bind(MessageService.class).annotatedWith(GmailServiceAnnotation.class).to(GmailService.class);
        bind(MessageService.class).annotatedWith(GoogleDriveServiceAnnotation.class).to(GoogleDriveService2LO.class);
        bind(MessageService.class).annotatedWith(LocalFileServiceAnnotation.class).to(LocalFileService.class);
        bind(MessageService.class).annotatedWith(AlfrescoServiceAnnotation.class).to(AlfrescoService.class);
        bind(MessageService.class).annotatedWith(TspIntegrationServiceAnnotation.class).to(TspIntegrationService.class);
    }

    @Provides
    @DefaultDssClientAnnotation
    public DssEslClient provideDefaultDssEslClient(
            @AlfrescoServiceAnnotation MessageService alfrescoService,
            @GmailServiceAnnotation MessageService gmailService,
            @LocalFileServiceAnnotation MessageService localFileService,
            @GoogleDriveServiceAnnotation MessageService googleDriveService,
            @TspIntegrationServiceAnnotation MessageService tspIntegrationService) {
        DefaultDssEslClient dssEslClient = new DefaultDssEslClient()
        dssEslClient.gmailService = gmailService
        dssEslClient.googleDriveService = googleDriveService
        dssEslClient.alfrescoService = alfrescoService
        dssEslClient.localFileService = localFileService
        dssEslClient.tspIntegrationService = tspIntegrationService
        return dssEslClient
    }

    @Provides
    @Singleton
    @GenericCallbackHandlerAnnotation
    public CallbackHandler provideGenericCallbackHandler(@Named("genericApiKey") String apiKey,
                                                         @Named("genericApiUrl") String apiUrl,
                                                         @Named("genericGoogleDriveFolder") String googleDriveFolderName,
                                                         @DefaultDssClientAnnotation DssEslClient dssEslClient,
                                                         @GenericPackageCompleteCallbackAnnontation Callback callback
    ) {
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        GenericCallbackHandler callbackHandler = new GenericCallbackHandler()
        callbackHandler.dssEslClient = dssEslClient
        callbackHandler.callback = callback
        callbackHandler.googleDriveFolderName = googleDriveFolderName
        callbackHandler
    }

    @Provides
    @Singleton
    @IacpCallbackHandlerAnnotation
    public CallbackHandler provideIacpCallbackHandler(@Named("iacpApiKey") String apiKey,
                                                      @Named("iacpApiUrl") String apiUrl,
                                                      @Named("iacpGoogleDriveFolder") String googleDriveFolderName,
                                                      @DefaultDssClientAnnotation DssEslClient dssEslClient,
                                                      @IacpPackageCompleteCallbackAnnontation Callback callback) {
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        IacpCallbackHandler callbackHandler = new IacpCallbackHandler()
        callbackHandler.dssEslClient = dssEslClient
        callbackHandler.callback = callback
        callbackHandler.googleDriveFolderName = googleDriveFolderName
        callbackHandler
    }

    @Provides
    @Singleton
    @TspCallbackHandlerAnnotation
    public CallbackHandler provideTspCallbackHandler(@Named("tspApiKey") String apiKey,
                                                     @Named("tspApiUrl") String apiUrl,
                                                     @Named("tspSendEmailToPackageOwnerOnPackageAttach") String tspSendEmailToPackageOwnerOnPackageAttach,
                                                     @Named("tspEmailSender") String tspEmailSender,
                                                     @Named("tspSendEmailToPackageOwnerOnPackageAttachBody") String tspSendEmailToPackageOwnerOnPackageAttachBody,
                                                     @Named("tspSendEmailToPackageOwnerOnPackageAttachSubject") String tspSendEmailToPackageOwnerOnPackageAttachSubject,
                                                     @Named("tspGoogleDriveFolder") String tspGoogleDriveFolder,
                                                     @DefaultDssClientAnnotation DssEslClient dssEslClient,
                                                     @TspPackageCompleteCallbackAnnotation Callback tspPackageComplete) {

        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        TspCallbackHandler callbackHandler = new TspCallbackHandler()
        callbackHandler.dssEslClient = dssEslClient
        callbackHandler.tspSendEmailToPackageOwnerOnPackageAttach = tspSendEmailToPackageOwnerOnPackageAttach
        callbackHandler.fromEmail = tspEmailSender
        callbackHandler.packageAttachEmailBody = tspSendEmailToPackageOwnerOnPackageAttachBody
        callbackHandler.packageAttachEmailSubject = tspSendEmailToPackageOwnerOnPackageAttachSubject
        callbackHandler.googleDriveFolderName = tspGoogleDriveFolder
        callbackHandler.tspPackageCompleteCb = tspPackageComplete
        callbackHandler
    }

    @Provides
    @Singleton
    @GenericPackageCompleteCallbackAnnontation
    public Callback provideGenericPackageCompleteCallback(@Named("genericApiKey") String apiKey,
                                                          @Named("genericApiUrl") String apiUrl,
                                                          @DefaultDssClientAnnotation DssEslClient dssEslClient) {
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        GenericPackageComplete callback = new GenericPackageComplete(dssEslClient)
        callback.dssEslClient = dssEslClient
        callback
    }

    @Provides
    @Singleton
    @IacpPackageCompleteCallbackAnnontation
    public Callback provideIacpPackageCompleteCallback(@Named("iacpApiKey") String apiKey,
                                                       @Named("iacpApiUrl") String apiUrl,
                                                       @DefaultDssClientAnnotation DssEslClient dssEslClient) {
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        IacpPackageComplete callback = new IacpPackageComplete(dssEslClient)
        callback.dssEslClient = dssEslClient
        callback
    }


    @Provides
    @TspPackageCompleteCallbackAnnotation
    public Callback provideTspPackageCompleteCallback(@Named("tspApiKey") String apiKey,
                                                      @Named("tspApiUrl") String apiUrl,
                                                      @Named("tspAgreementFilename") String tpiAgreementFilename,
                                                      @Named("tspSpreadSheetFilename") String tpiSpreadSheetFilename,
                                                      @Named("tspEmailSender") String fromEmail,
                                                      @Named("tspEmailSubject") String subject,
                                                      @Named("tspEmailBody") String body,
                                                      @Named("tspSendEmailToPackageOwnerOnPackageComplete") String sendEmailToPackageOwnerOnPackageComplete,
                                                      @DefaultDssClientAnnotation DssEslClient dssEslClient) {
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
        TspPackageComplete callback = new TspPackageComplete(dssEslClient)
        callback.tpiAgreementFilename = tpiAgreementFilename
        callback.tpiSpreadSheetFilename = tpiSpreadSheetFilename
        callback.fromEmail = fromEmail
        callback.subject = subject
        callback.body = body
        callback.sendEmailToPackageOwnerOnPackageComplete = sendEmailToPackageOwnerOnPackageComplete
        callback.dssEslClient = dssEslClient
        callback
    }

}
