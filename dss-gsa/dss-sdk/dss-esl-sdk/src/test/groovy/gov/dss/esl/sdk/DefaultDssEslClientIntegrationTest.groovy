package gov.dss.esl.sdk

import com.google.inject.Guice
import com.google.inject.Inject
import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.DocumentPackageAttributes
import com.silanis.esl.sdk.EslClient
import com.silanis.esl.sdk.PackageId
import gov.dss.esl.sdk.annontations.DefaultDssClientAnnotation
import gov.dss.esl.sdk.module.TestGuiceModule
import gov.dss.esl.sdk.utilities.service.DSSQueueManagement
import org.apache.commons.codec.binary.Base64
import org.apache.commons.io.IOUtils
import org.junit.Ignore

import static com.silanis.esl.sdk.builder.DocumentBuilder.newDocumentWithName
import static com.silanis.esl.sdk.builder.PackageBuilder.newPackageNamed
import static com.silanis.esl.sdk.builder.SignatureBuilder.signatureFor
import static com.silanis.esl.sdk.builder.SignerBuilder.newSignerWithEmail
import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*

/**
 *
 * DSSCR-125 Bulk Identities
 *
 * Created by Carvel on 11/21/15 @ 4:03 PM.
 *
 * TODO: This needs to be refactored to use EasyMock.
 *
 */
@Ignore
class DefaultDssEslClientIntegrationTest extends GroovyTestCase {
    def user1 = "DSSCR125.1@gmail.com" //---password !QAZxsw2125
    def user2 = "DSSCR125.2@gmail.com" //---password !QAZxsw2125

    @Inject
    @DefaultDssClientAnnotation
    DssEslClient dssEslClient

    void setUp() {
        Guice.createInjector(new TestGuiceModule(this))
    }

    void setUpDssEslClient() {
        String apiKey = "Y3h1Y3ExYjhaenNDOjM3RHlTTlI2SmdJRw==";
        String apiUrl = "https://sandbox.e-signlive.com/api"
        dssEslClient.setDelegate(new EslClient(apiKey, apiUrl))
    }

    void testDependencyInjection() {
        //--- make assertions
        assert dssEslClient != null
        assert ((DefaultDssEslClient) dssEslClient).delegate != null
        assert ((DefaultDssEslClient) dssEslClient).gmailService != null
        assert ((DefaultDssEslClient) dssEslClient).googleDriveService != null
    }

    void testCreateIdentitiesWithJson() {
        //--- set up test data
        //--- exercise method
        String result = dssEslClient.createIdentities(getIdentities("dsscr125-identities.json"))

        //--- make assertions
        assert result != null
    }

    void testCreateIdentitiesWithExcel() {
        //--- exercise method
        String[] result = dssEslClient.createIdentities(new FileInputStream(getIdentities("dsscr188-identities.xlsx")))

        //--- make assertions
        assert result != null
    }

    void testBasicPackageCreation() {
        DocumentPackage documentPackage = newPackageNamed("DSS SDK TEST PACKAGE A")
                .withSigner(newSignerWithEmail("dssdeveloper.sudhangi@gmail.com")
                .withCustomId("Manager")
                .withFirstName("signerFirstName")
                .withLastName("signerLastName"))
                .withSigner(newSignerWithEmail("dssdeveloper.ch@gmail.com")
                .withFirstName("yourFirstName")
                .withLastName("yourLastName"))
                .withDocument(newDocumentWithName("sampleAgreement")
                .fromFile(getSampleAgreementFilePath())
                .withSignature(signatureFor("dss.gsa.developer@gmail.com")
                .onPage(0)
                .atPosition(175, 165))
                .withSignature(signatureFor("dssdeveloper.ch@gmail.com")
                .onPage(0)
                .atPosition(550, 165)))
                .build()

        //--- exercise methods

        // Issue the request to the e-SignLive server to create the DocumentPackage
        PackageId packageId = dssEslClient.createPackage(documentPackage)

        // Send the package to be signed by the participants
        dssEslClient.sendPackage(packageId)
    }

    void testInsertSignatureBlock() {
        setUpDssEslClient();

        long startTime = System.currentTimeMillis();
        //--- set up test data
        InputStream documentInputStream1 = this.getClass().getResourceAsStream("TEST-AAAP-Short.PDF");
        String docName = ("C:/fakepath/TEST-AAAP-Short.PDF");
        List<String> listSignerEmails = new ArrayList<String>();
        List<String> listTexts = new ArrayList<String>();
        List<String> listSignTypes = new ArrayList<String>();
        List<String> listFirstName = new ArrayList<String>();
        List<String> listLastName = new ArrayList<String>();

        listSignerEmails.add("sudhangi@gmail.com");
        listSignerEmails.add("dssdeveloper.sudhangi@gmail.com");

        listTexts.add("LESSOR:");
        listTexts.add("GOVERNMENT:");

        listSignTypes.add("captureFor");
        listSignTypes.add("initialsFor");

        listFirstName.add("Name1");
        listFirstName.add("Name2");

        listLastName.add("LastName1");
        listLastName.add("LastName2");

        //--- exercise methods

        // Issue the request to the e-SignLive server to create the DocumentPackage
        String packageId = dssEslClient.insertSignatureBlock(documentInputStream1,
                docName,
                "sudhangi.ambekar@icfi.com",
                "SignatureInsertionTest",
                listSignerEmails,
                listTexts,
                listSignTypes,
                listFirstName,
                listLastName,
                "assignOrder",
                "createSend");

        assert packageId != null

//        long endTime = System.currentTimeMillis();
//        long totalTime = endTime - startTime;
//        System.out.println("Total Time of Execution: " + totalTime / 1000);
//        System.out.println("Test Complete. Package id is: " + packageId);

    }

    private File getIdentities(String json) {
        URL filenameUrl = this.class.getResource(json)
        URI uri = filenameUrl.toURI()
        new File(uri)
    }

    void testValidateData() {
        setUpDssEslClient();
        InputStream inputStream1 = this.getClass().getResourceAsStream("TEST-AAAP-Short.PDF");
        InputStream inputStream2 = this.getClass().getResourceAsStream("TEST-CDT-Short.pdf");
        //        encode the inputstream into base64 string
        def bytes = IOUtils.toByteArray(inputStream1);
        def bytes64 = Base64.encodeBase64(bytes);
        def content1 = new String(bytes64);

        bytes = IOUtils.toByteArray(inputStream2);
        bytes64 = Base64.encodeBase64(bytes);
        def content2 = new String(bytes64);

        def signerMap1 = [signerEmail      : "dssdeveloper.sudhangi@gmail.com",
                          signerFirstName  : "Dss",
                          signerLastName   : "Developer",
                          searchText       : "LESSOR",
                          signType         : "ClickToInitial",
                          signaturePosition: "TOPRIGHT"]
        def signerMap2 = [signerEmail      : "dssdev11@gmail.com",
                          signerFirstName  : "dss",
                          signerLastName   : "One",
                          searchText       : "GOVERNMENT",
                          signType         : "CaptureSignature",
                          signaturePosition: "BOTTOMRIGHT"]
        def signers1 = [signerMap1, signerMap2]

        signerMap1 = [signerEmail      : "dssdev12@gmail.com",
                      signerFirstName  : "Dss",
                      signerLastName   : "Two",
                      searchText       : "TextCDT",
                      signType         : "ClickToInitial",
                      signaturePosition: "TOPRIGHT"]
        signerMap2 = [signerEmail      : "dssdev13@gmail.com",
                      signerFirstName  : "Dss",
                      signerLastName   : "Three",
                      searchText       : "StringCDT",
                      signType         : "CaptureSignature",
                      signaturePosition: "BOTTOMRIGHT"]
        def signers2 = [signerMap1, signerMap2]

        def documentMap1 = [documentName: "TEST_AAAP.PDF", documentContent: content1, signers: signers1]
        def documentMap2 = [documentName: "CDT.pdf", documentContent: content2, signers: signers2]

        def documents1 = [document: documentMap1]
        def documents2 = [document: documentMap2]
        def documents = [documents1, documents2]
        /** Test 1
         * The code returns the following HashMap<String,Object>. This is the 'happy path' test case.
         */
        def allData = [orgName           : "AAAP",
                       senderEmail       : "sudhangi.ambekar@icfi.com",
                       packageName       : "SignatureBlockInserted",
                       enableSigningOrder: false,
                       packageOption     : "createSend",
                       documents         : documents]

        def result = dssEslClient.validateData(allData);
        assert result != null
//        System.out.println("Result for Test 1:" + result)

        /** Test 2
         * This will return an error because it is the 'Package name missing' test
         */

        allData = [orgName           : "AAAP",
                   senderEmail       : "",
                   packageName       : "UnitTestValidateData",
                   enableSigningOrder: false,
                   packageOption     : "",
                   documents         : documents]

        result = dssEslClient.validateData(allData);
        assert result != null
//        System.out.println("Result for Test 2:" + result)
    }

    void testDssUniversalConnector() {
        setUpDssEslClient();
        InputStream inputStream1 = this.getClass().getResourceAsStream("TEST-AAAP-Short.PDF");
        InputStream inputStream2 = this.getClass().getResourceAsStream("TEST-CDT-Short.pdf");

//        encode the inputstream into base64 string
        def bytes = IOUtils.toByteArray(inputStream1);
        def bytes64 = Base64.encodeBase64(bytes);
        def content1 = new String(bytes64);

        bytes = IOUtils.toByteArray(inputStream2);
        bytes64 = Base64.encodeBase64(bytes);
        def content2 = new String(bytes64);


        def signerMap1 = [signerEmail      : "dssdeveloper.sudhangi@gmail.com",
                          signerFirstName  : "Dss",
                          signerLastName   : "Developer",
                          searchText       : "",
                          signType         : "ClickToInitial",
                          signaturePosition: "TOPRIGHT"]
        def signerMap2 = [signerEmail      : "dssdev11@gmail.com",
                          signerFirstName  : "dss",
                          signerLastName   : "One",
                          searchText       : "GOVERNMENT",
                          signType         : "CaptureSignature",
                          signaturePosition: "BOTTOMRIGHT"]
        def signers1 = [signerMap1, signerMap2]

        signerMap1 = [signerEmail      : "dssdev12@gmail.com",
                      signerFirstName  : "Dss",
                      signerLastName   : "Two",
                      searchText       : "TextCDT",
                      signType         : "ClickToInitial",
                      signaturePosition: "TOPRIGHT"]
        signerMap2 = [signerEmail      : "dssdev13@gmail.com",
                      signerFirstName  : "Dss",
                      signerLastName   : "Three",
                      searchText       : "StringCDT",
                      signType         : "CaptureSignature",
                      signaturePosition: "BOTTOMRigHT"]
        def signers2 = [signerMap1, signerMap2]

        def documentMap1 = [documentName: "AAAP.PDF", documentContent: content1, signers: signers1]
        def documentMap2 = [documentName: "CDT.PDF", documentContent: content2, signers: signers2]

        def documents1 = [document: documentMap1]
        def documents2 = [document: documentMap2]
        def documents = [documents1, documents2]
        /**
         * The code returns the following HashMap<String,Object>.
         */
        def allData = [orgName           : "AAAP",
                       senderEmail       : "sudhangi.ambekar@icfi.com",
                       packageName       : "UnitTestDssUnivConn",
                       enableSigningOrder: false,
                       packageOption     : "create",
                       documents         : documents]


        def result = dssEslClient.dssUniversalConnector(allData);
        assert result != null
//        System.out.println("result:" + result)
    }

    void testGetPackageAttribute() {
        setUpDssEslClient();
        PackageId packageId = new PackageId("9f69f40d-61d2-48d5-a1ef-5d865492ccdf");
        def documentPackage = dssEslClient.getPackage(packageId);
        DocumentPackageAttributes documentPackageAttributes = documentPackage.getAttributes();
        assertThat("The OrgName is not Null", documentPackageAttributes, is(notNullValue()));
        assertThat("The OrgName was set correctly", documentPackageAttributes.getContents().get("orgName").toString(), is(equalTo("AAAP")));

        //println documentPackageAttributes.getContents().get("orgName").toString();
    }

    private String getSampleAgreementFilePath() {
        URL sampleAgreementUrl = this.class.getResource("sampleAgreement.txt")
        URI uri = sampleAgreementUrl.toURI()
        File file = new File(uri)
        file.absolutePath
    }


    void testDSSQueueManagement() {

        String mymessage = "{\n" +
                "  \"orgName\": \"your Organization Name\",\n" +
                "  \"notificationType\": \"PACKAGE_COMPLETE\",\n" +
                "  \"packageInfo\": {\n" +
                "    \"packageId\": \"YTHT55666\",\n" +
                "    \"packageName\": \"SampleTestPackage\"\n" +
                "  }\n" +
                "}"

        DSSQueueManagement dssqueue = new DSSQueueManagement()

        dssqueue.initConnection()

        def result = dssqueue.publishToQueue("DSS_RETA_QUEUE_DEV", mymessage);
        System.out.println ("published to queue: " + result);

        result = dssqueue.retrieveFromQueue("DSS_RETA_QUEUE_DEV");
        System.out.println ("retrieved from queue: " + result);
        dssqueue.closeConnection()



    }

    void testRetrieveDSSQueueMessage() {

       DSSQueueManagement dssqueue = new DSSQueueManagement()

        dssqueue.initConnection()

        def result = dssqueue.retrieveFromQueue("DSS_RETA_QUEUE_DEV");
        System.out.println ("retrieved from queue: " + result);
        dssqueue.closeConnection()
    }

}
