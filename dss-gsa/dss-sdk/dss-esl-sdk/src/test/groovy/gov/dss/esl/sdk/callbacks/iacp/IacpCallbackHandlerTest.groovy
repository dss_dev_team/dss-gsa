package gov.dss.esl.sdk.callbacks.iacp

import com.google.inject.Inject
import gov.dss.esl.sdk.annontations.IacpCallbackHandlerAnnotation
import gov.dss.esl.sdk.callbacks.CallbackHandler
import gov.dss.esl.sdk.module.TestGuiceModule
import gov.dss.sdk.annotations.AlfrescoServiceAnnotation
import gov.dss.sdk.service.MessageService
import org.junit.Ignore

import static com.google.inject.Guice.createInjector
import static gov.dss.sdk.service.MessageService.*

/**
 * Test case to exercise the IacpCallbackHandler
 *
 * Created by carvelhall on 6/1/16.
 */
@Ignore
class IacpCallbackHandlerTest extends GroovyTestCase {
    @Inject
    @IacpCallbackHandlerAnnotation
    CallbackHandler iacpCallbackHandler
    String filename = "DssTest.pdf"

    @Inject
    @AlfrescoServiceAnnotation
    MessageService alfrescoService


    String json = "{\"@class\":\"com.silanis.esl.packages.event.ESLProcessEvent\",\"name\":\"PACKAGE_COMPLETE\",\"sessionUser\":\"3c79375d-fc09-439e-a4d5-3b075c32aa0b\",\"packageId\":\"cabdeaf9-ba82-4ace-a09c-2bb26d58d2d4\",\"message\":null,\"documentId\":null}"

    void setUp() {
        createInjector(new TestGuiceModule(this));
        alfrescoService.sendMessage([(ACTION): (DELETE), (FILE_NAME): filename])
    }

    void testDependencyInject() {
        assert iacpCallbackHandler != null
        assert alfrescoService != null
    }

    void testHandleCallback() {
        iacpCallbackHandler.handleCallback(json)
    }
}
