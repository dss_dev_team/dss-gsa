package gov.dss.esl.sdk.callbacks

import com.google.inject.Inject
import gov.dss.esl.sdk.annontations.IacpPackageCompleteCallbackAnnontation
import gov.dss.esl.sdk.module.TestGuiceModule
import gov.dss.sdk.service.AlfrescoService
import org.junit.Ignore

import static com.google.inject.Guice.createInjector
import static gov.dss.sdk.service.MessageService.*

/**
 * Author : Carvel
 * Date   : 4/27/16
 */
@Ignore
class IacpPackageCompleteTest extends GroovyTestCase {
    @Inject
    @IacpPackageCompleteCallbackAnnontation
    Callback callback

    @javax.inject.Inject
    AlfrescoService alfrescoService
    String filename = "DssTest.pdf"

    void setUp() {
        createInjector(new TestGuiceModule(this));
        alfrescoService.sendMessage([(ACTION): (DELETE), (FILE_NAME): filename])
    }

    void testDependencyInject() {
        assert callback != null
        assert callback.dssEslClient != null
        assert callback.dssEslClient.delegate != null
    }

    void testPackageComplete() {
        //--- set up test data
        String packageId = "cabdeaf9-ba82-4ace-a09c-2bb26d58d2d4"
        callback.packageId = packageId

        //--- exercise method
        callback.handle()

        //--- make assertions
    }
}
