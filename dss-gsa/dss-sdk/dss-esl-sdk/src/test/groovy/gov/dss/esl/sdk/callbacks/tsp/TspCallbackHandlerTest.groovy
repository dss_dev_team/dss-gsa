package gov.dss.esl.sdk.callbacks.tsp

import com.google.inject.Inject
import gov.dss.esl.sdk.annontations.TspCallbackHandlerAnnotation
import gov.dss.esl.sdk.callbacks.CallbackHandler
import gov.dss.esl.sdk.module.TestGuiceModule

import static com.google.inject.Guice.createInjector

/**
 * Created by carvelhall on 6/5/16.
 */
class TspCallbackHandlerTest extends GroovyTestCase {
    @Inject
    @TspCallbackHandlerAnnotation
    CallbackHandler tspCallbackHandler
    String filename = "DssTest.pdf"

    String json = "{\"@class\":\"com.silanis.esl.packages.event.ESLProcessEvent\",\"name\":\"PACKAGE_COMPLETE\",\"sessionUser\":\"1e769035-3afe-4d2b-ad91-203ce0144094\",\"packageId\":\"1cde983b-8508-44f9-b79a-fa78dfe69df5\",\"message\":null,\"documentId\":null}"

    void setUp() {
        createInjector(new TestGuiceModule(this));
    }

    void testDependencyInject() {
        assert tspCallbackHandler != null
        assert tspCallbackHandler.dssEslClient != null
        assert tspCallbackHandler.tspPackageCompleteCb != null
        assert tspCallbackHandler.tspPackageCompleteCb.dssEslClient != null
    }

    void testHandleCallback() {
        tspCallbackHandler.handleCallback(json)
    }
}
