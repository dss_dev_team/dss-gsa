<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-12" id="hero">
        <h4>GSA Admin</h4>

        <h2>Signature Block Insertion</h2>

        <p>Please choose an Application.</p>
        <table>
            <tr>
                <th>Application</th>
                <th>Description</th>
            </tr>
        <tr>
            <td>
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a
                            href="${uploadSignatureInsertionAAAPUrl}">AAAP</a>
                    </li>
                </ul>
            </td>
            <td>Automated Advanced Acquisition Program (AAAP) Signature Insertion</td>
        </tr>
        <tr>
            <td>
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a
                            href="${uploadSignatureInsertionCDTUrl}">CDT</a>
                    </li>
                </ul>
            </td>
            <td>Control Document Tracker (CDT) Signature Insertion</td>
        </tr>
        </table>
    </div>
</div>
</@layout.default>