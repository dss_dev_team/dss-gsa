<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-12" id="hero">
        <h4>GSA Admin</h4>

        <h2>Signing Ceremony for Control Document Tracker(CDT)</h2>

        <p>Please choose a PDF file to insert signatures and specify the search string and email-id of the signer and
            submit.</p>

        <form method="post" action="../views/esl/accounts/uploadSignatureInsertion" enctype="multipart/form-data">
            <input type="file" id="documentid" name="doc"
                   onchange="document.getElementsByName('documentName')[0].value = document.getElementById('documentid').value;"
                   required>
            <input type="hidden" name="documentName" value="">

            <p>Package Owner E-mail Id:</p>
            <input class="form-control" type="text" name="ownerEmail" required>

            <p>Package Name:</p>
            <input class="form-control" type="text" name="newPackageName" required>

            <p>Signers Table:</p>

            <INPUT type="button" value="Add Row" onclick="addRow('dataTable')"/>

            <INPUT type="button" value="Delete Row" onclick="deleteRow('dataTable')"/>

            <TABLE id="dataTableHeader" width="450px" border="1">
                <TR>
                    <TH>&nbsp;&nbsp;</TH>
                    <TH>Signer's
                        Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TH>
                    <TH>Signer's First Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TH>
                    <TH>Signer's Last Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TH>
                    <TH>Search Text&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TH>
                    <TH>Signature Type</TH>
                </TR>
            </TABLE>
            <TABLE id="dataTable" width="350px" border="1">
                <TR>
                    <TD><INPUT type="checkbox" name="chk"/></TD>
                    <TD><INPUT type="text" name="tablesignersEmail" required/></TD>
                    <TD><INPUT type="text" name="tablesignersFirstName" required/></TD>
                    <TD><INPUT type="text" name="tablesignersLastName" required/></TD>
                    <TD>
                        <SELECT name="tabletexts">
                            <OPTION value="TextCDT:">TextCDT</OPTION>
                            <OPTION value="StringCDT:">StringCDT</OPTION>
                        </SELECT>
                    </TD>
                    <TD>
                        <SELECT name="tablesignType">
                            <OPTION value="signatureFor">Click-to-Sign</OPTION>
                            <OPTION value="initialsFor">Click-to-Initial</OPTION>
                            <OPTION value="captureFor">Capture Signature</OPTION>
                            <OPTION value="mobileCaptureFor">Mobile Signature</OPTION>
                        </SELECT>
                    </TD>
                </TR>
            </TABLE>
            <input type="checkbox" name="assignOrder" value="assignOrder"> Enable Signing Order<br>

            <p></p>
            <p></p>

            <input type="radio" name="createSend" value="onlyCreate" checked> Create Package<br>
            <input type="radio" name="createSend" value="createSend"> Create and Send Package<br>

            <p>
                <input class="form-control" type="submit">
            </p>
            <iframe src="https://sandbox.e-signlive.com/packages/inbox" width="500" height="300"></iframe>

        </form>
    </div>
</div>
</@layout.default>