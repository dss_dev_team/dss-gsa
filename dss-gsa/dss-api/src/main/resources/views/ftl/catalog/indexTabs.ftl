<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-10" id="hero">
        <h2>Digital Signature Solution(DSS)</h2>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">About DSS</a></li>
            <li><a data-toggle="tab" href="#menu1">Service Catalog</a></li>
            <li><a data-toggle="tab" href="#menu2">Demo Links</a></li>
            <li><a data-toggle="tab" href="#menu3">Contacts</a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <iframe src="https://docs.google.com/document/d/11CwAq75rIUUHS5hiASTL4gngD1CEWAQkJYNjYXN2E9k/pub?embedded=true" width="1000" height="750"></iframe>
            </div>
            <div id="menu1" class="tab-pane fade">
                <p></p>
                <iframe src="https://docs.google.com/spreadsheets/d/1V38-T-9WKj_SRKmhgNkOA5vVsd0uqni2AdpIQgb0SAw/pubhtml?widget=false&amp;headers=false" width="1000" height="1000"></iframe>
            </div>
            <div id="menu2" class="tab-pane fade">
                <iframe src="https://docs.google.com/spreadsheets/d/1p44KKZpFdFbborXPt647VY8nuTKHbaaHkMVywQQ2QqU/pubhtml?widget=true&amp;headers=false" width="500" height="500"></iframe>
            </div>
            <div id="menu3" class="tab-pane fade">
                <iframe src="https://docs.google.com/document/d/1Rldr_A636wTV_e7NM4ch4Vc_eatXNVGeUorEdu0WD2M/pub?embedded=true" width="500" height="500"></iframe>

            </div>
        </div>
    </div>
</div>
</@layout.default>