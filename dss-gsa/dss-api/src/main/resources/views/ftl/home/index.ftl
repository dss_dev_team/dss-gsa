<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<style>
    a {
        color: white
    }
</style>
<div class="col-sm-8 col-md-10">
    <div class="container" id="page-content" role="main" tabindex="-1">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a data-toggle="tab" href="#">About DSS</a></li>
            <li role="presentation"><a data-toggle="tab" href="#">Service Catalog</a></li>
            <li role="presentation"><a data-toggle="tab" href="#">Demo Links</a></li>
        </ul>
    </div>
</div>
</@layout.default>
