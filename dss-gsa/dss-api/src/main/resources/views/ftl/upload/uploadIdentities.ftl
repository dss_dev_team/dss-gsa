<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-10" id="hero">
        <h4>GSA Admin</h4>

        <h2>Bulk Identities</h2>

        <p>Please choose a Microsoft Excel file containing identity information and submit.</p>

        <form method="post" action="../views/esl/accounts/uploadIdentities" enctype="multipart/form-data">
            <input class="form-control" type="file" name="identities">
            <p>
                <input class="form-control" type="submit">
            </p>
        </form>
    </div>
</div>
</@layout.default>
