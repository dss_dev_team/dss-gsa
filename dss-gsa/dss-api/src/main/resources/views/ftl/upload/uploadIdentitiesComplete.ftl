<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-10" id="hero">
        <h4>GSA Admin</h4>

        <h2>Bulk Identities</h2>

        <p>
            <a href="${eslApiUrl}"><span class="btn gsa-primary-btn">Go To e-SignLive</span></a>
            <a href="${uploadIdentityUrl}"><span class="btn gsa-primary-btn">Upload more identities</span></a>
        </p>
        <ul class="list-group">
            <#list messages as message>
                <#if message?starts_with("Success")>
                    <li class="list-group-item list-group-item-success">${message}</li>
                <#else>
                    <li class="list-group-item list-group-item-danger">${message}</li>
                </#if>
            </#list>
        </ul>
    </div>
</div>
</@layout.default>
