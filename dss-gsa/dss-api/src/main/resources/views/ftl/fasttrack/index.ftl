<#import "../layout/defaultLayout.ftl" as layout>
<@layout.default>
<div class="container" id="page-content" role="main" tabindex="-1">
    <div class="col-sm-8 col-md-10" id="hero">
        <h2>Transportation Service Providers Freight Form</h2>

        <div class="alert alert-info" role="alert">
            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="https://sandbox.e-signlive.com/u/yW">Goto
                    Form</a></li>
            </ul>
        </div>
    </div>
</div>
</@layout.default>