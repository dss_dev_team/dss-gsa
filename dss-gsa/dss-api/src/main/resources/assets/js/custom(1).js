if (!GSA) {
    var GSA = {}
}
//needs to be global scope for now

dataLayer = [];

var gaCrumb,
    gaPrintMethod = 'other';

//scoped to GSA obj
GSA.analytics = function () {
    gaCrumb = [];
    if ($('.breadcrumb').length > 0) {
        $('.breadcrumb a').each(function () {
            var crumbText = $(this).text();
            if (crumbText.toLowerCase() !== 'home') {
                gaCrumb.push(crumbText);
            }
        });
        var gaTitle = document.title.replace(/[^a-zA-Z\d\s]/g, '').toLowerCase();
        gaCrumb = '/' + gaCrumb.join('/').replace(/[^a-zA-Z\d\s\/]/g, '').toLowerCase() + '/' + gaTitle;
        gaCrumb = gaCrumb.replace(/ +/g, '-')
    } else {
        gaCrumb = '/';
    }
    if (window.location.hash.indexOf('#/') !== -1) {
        gaCrumb = gaCrumb + '/' + window.location.hash;
    }

    dataLayer.push({
        'breadcrumb': gaCrumb
    });

    $(window).hashchange(function () {
        if (window.location.hash.indexOf('#/') > -1) {
            if (gaCrumb.indexOf('#') > -1) {
                gaCrumb = gaCrumb.split('#')[0];
            }
            ga('send', 'pageview', gaCrumb + '/' + window.location.hash);
        }
    });
}

GSA.images = new function () {
    this.imgResponsive = function () {
        $('img').each(function () {
            $(this).addClass('img-responsive');
        });
    }
};

GSA.hacks = new function () {
    this.iframeWrapper = function () {
        $('iframe').each(function () {
            $container = $(this).parent('div').attr('class')
            if ($container != 'embed-responsive' && $container != 'exclude-embed-responsive') {
                $(this).wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
            }
            ;
        });
    }
};
// vertical alignment plugin
jQuery.fn.verticalAlign = function () {
    return this
        .css("padding-top", ($(this).parent().height() - $(this).height()) / 2 + 'px').css("padding-bottom", ($(this).parent().height() - $(this).height()) / 2 + 'px')
};

GSA.rotatingFeatureBlocks = function () {
    var $rfb = $('.carousel');
    $rfb.each(function () {
        $(this).carousel({
            interval: 9000,
            wrap: false
        });
    });
}

GSA.print = function () {
    $('body').on('click', '.print-page', function (e) {
        e.preventDefault();
        printDirective();
    });
    // legacy id
    $('body').on('click', '#btnPrinter', function (e) {
        e.preventDefault();
        printDirective();
    });
    // perdiem
    $('body').on('click', '#print-perdiemResults', function (e) {
        e.preventDefault();
        PrintElem('#perdiem-results', 'Per Diem Results')
    });

}
GSA.indexWidget = function (path, targetListID, parentID, dateClass, linkClass, titleClass, additionalClass) {
    var location = window.location.protocol + "//" + window.location.host + "/";
    $.ajax({
        url: location + path,
        success: function (result) {
            $(targetListID).parent('div').find('.vertical-block-loading').fadeOut(300, function () {
                $(this).remove();
            });
            var html = $('<div>').html(result);
            var eventRows = html.find(parentID).find('tr');
            eventRows.each(function (i) {
                var date = $(this).find(dateClass).text(),
                    link = $(this).find(linkClass).attr('href'),
                    title = $(this).find(titleClass).text(),
                    additional = $(this).find(additionalClass).text();

                var original = new Date(date);
                if (moment(date) > 0) {
                    var month = moment(date).format('MMM');
                    var day = moment(date).format('D');
                    date = month + '<span>' + day + '</span>';
                } else {
                    var month = moment(date, 'dddd, MMMM D').format('MMM');
                    var day = moment(date, 'dddd, MMMM D').format('D');
                    date = month + '<span>' + day + '</span>';
                }
                if (additional.length > 0) {
                    var template = '<li class="rss-feeds-entry">' +
                        '<div class="row">' +
                        '<div class="col-xs-2">' +
                        '<small>' + date + '</small>' +
                        '</div>' +
                        '<div class="col-xs-10">' +
                        '<a href="' + link + '">' + title + '</a>' +
                        '<p><span class="icon-location"></span> ' + additional + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                } else {
                    var template = '<li class="rss-feeds-entry">' +
                        '<div class="row">' +
                        '<div class="col-xs-2">' +
                        '<small>' + date + '</small>' +
                        '</div>' +
                        '<div class="col-xs-10">' +
                        '<a href="' + link + '">' + title + '</a>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                }
                if (link != undefined) {
                    $(targetListID).find('ul').append(template);
                    return i < 6;
                } else {
                    return i--;
                }
                ;
            });
        }
    });
};
GSA.initVerticalCarousel = function (targetListID, height) {
    var item_height = height;
    var visible_items = '3';
    var viewport_height = item_height * visible_items;
    $(targetListID).find('ul > li').remove();
    $('.rss-feeds-entry').each(function (i) {
        if (i == 0) {
            $(this).addClass('first-item')
        }
        $(this).height(parseInt(item_height) + 'px');
    });
    $(targetListID).find('.viewport').height(parseInt(viewport_height) + 'px');
    setInterval(function () {
        if (!$('#main-nav > li').hasClass('open')) {
            $(targetListID).find('.slide-next').click();
        }
        ;
    }, 10000);
    $(targetListID).infiniteCarousel({
        itemsPerMove: 1,
        duration: 100,
        vertical: true
    });
};
GSA.sustainability = new function () {
    this.blockHover = function () {
        $('.sustainability-block').hover(function () {
            $(this).addClass('on');
        }, function () {
            $(this).removeClass('on');
        })
    };
}


function printDirective() {
    $('#inner-main-navigation, #footer, #footer-bar, #left-sidebar, .addthis_toolbox').css('display', 'none');
    $('body').prepend('<img width="286" height="50" id="logo-print" border="0" src="/resources/images/GSAlogo.gif">')
    setTimeout(function () {
        window.print();
    }, 750);
    $('#inner-main-navigation, #footer, #footer-bar, #left-sidebar, .addthis_toolbox').show();
    $('#logo-print').remove();
}
function PrintElem(elem, title) {
    Popup($(elem).html(), title);
}

function Popup(data, title) {
    var mywindow = window.open('', title, 'height=600,width=900');
    mywindow.document.write('<html><head><title>' + title + '</title>');
    //mywindow.document.write('<link rel="stylesheet" href="all.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}

// Doc Ready -------
$(function () {
    GSA.hacks.iframeWrapper();

    /*
     GSA.hacks.fixesForIE8();
     GSA.analytics();
     GSA.print();

     GSA.navigations.toggleMenu();
     GSA.navigations.hoverTransitions();
     GSA.navigations.categoryNav();
     GSA.navigations.searchToggle();
     GSA.navigations.searchToggleMobile();
     GSA.navigations.tabToContent();
     GSA.navigations.desktopNavTemplate();
     GSA.navigations.mobileNavTemplate();
     GSA.navigations.footerNavTemplate();
     GSA.navigations.mobileBlueBar();
     GSA.navigations.highlightTopLevel();
     GSA.navigations.navFeaturedContent();
     GSA.navigations.navAlign();


     GSA.images.imgResponsive();
     GSA.hacks.homepage();
     GSA.hacks.verticalBuckets();
     GSA.hacks.tabNavigation();
     GSA.hacks.responsiveTables();
     GSA.hacks.inPageAnchors();
     GSA.hacks.leftSidebarNotNav();
     GSA.buckets.featuredArticles();

     validate();
     $('#perdiemSearchVO_state, #perdiemSearchVO_zip').change(validate);

     if ($('.carousel').length > 0) {
     GSA.rotatingFeatureBlocks();
     }

     $(window).resize(function () {
     GSA.hacks.leftSidebarNotNav();
     });
     */
});