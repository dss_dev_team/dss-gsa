package gov.dss.api.views

import io.dropwizard.views.View

/**
 * Author : Carvel 
 * Date   : 4/18/16
 */
class UploadSignatureInsertionView extends View {
    String uploadSignatureInsertionAAAPUrl
    String uploadSignatureInsertionCDTUrl
    String uploadSignatureInsertionUrl

    /**
     * Creates a new view.
     *
     * @param templateName the name of the template resource
     */
    public UploadSignatureInsertionView(String templateName, String uploadSignatureInsertionAAAPUrl, String uploadSignatureInsertionCDTUrl, String uploadSignatureInsertionUrl) {
        super(templateName)
        this.uploadSignatureInsertionAAAPUrl = uploadSignatureInsertionAAAPUrl
        this.uploadSignatureInsertionCDTUrl = uploadSignatureInsertionCDTUrl
        this.uploadSignatureInsertionUrl = uploadSignatureInsertionUrl
    }
}
