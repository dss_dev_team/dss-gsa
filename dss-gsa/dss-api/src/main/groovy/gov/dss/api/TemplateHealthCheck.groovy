package gov.dss.api

import com.codahale.metrics.health.HealthCheck

/**
 * Author : Carvel 
 * Date   : 6/29/16
 */

public class TemplateHealthCheck extends HealthCheck {

    public TemplateHealthCheck() {
    }

    @Override
    protected com.codahale.metrics.health.HealthCheck.Result check() throws Exception {
        return com.codahale.metrics.health.HealthCheck.Result.healthy();
    }
}
