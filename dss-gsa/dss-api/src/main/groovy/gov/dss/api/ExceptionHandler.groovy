package gov.dss.api

import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

/**
 * Created by Carvel
 *
 * DSSCR-503: Abstraction for handling all exceptions thrown from th drop wizard web app
 *
 * This abstraction will
 * 1.) catch exceptions
 * 2.) print the stacktrace
 * 3.) return a status of 200 therefore, preventing failure emails from being sent to
 *     an eSignLive package owner.
 */
@Provider
class ExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    Response toResponse(Exception e) {
        e.printStackTrace()
        final Response.ResponseBuilder responseBuilder = Response.status(200);
        responseBuilder.build()
    }
}
