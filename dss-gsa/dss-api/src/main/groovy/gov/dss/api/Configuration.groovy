package gov.dss.api

import com.fasterxml.jackson.annotation.JsonProperty
import com.google.common.collect.ImmutableMap

import javax.validation.constraints.NotNull

/**
 * The Dropwizard configuration for DSS
 *
 * Please see http://www.dropwizard.io/ for details
 *
 * Created by Carvel on 12/28/15 @ 1:09 PM.
 */
class Configuration extends io.dropwizard.Configuration {
    @NotNull
    private String catalogUrl

    @NotNull
    private String fastTrackUrl

    @NotNull
    private String dssPlatformHost

    @NotNull
    private String dssPlatformPort

    @NotNull
    private String eslApiUrl

    @NotNull
    private String uploadIdentityUrl

    @NotNull
    private String uploadSignatureInsertionUrl

    @NotNull
    private String uploadSignatureInsertionAAAPUrl

    @NotNull
    private String uploadSignatureInsertionCDTUrl

    @NotNull
    private Map<String, Map<String, String>> viewRendererConfiguration = Collections.emptyMap();

    @JsonProperty("eslApiUrl")
    String getEslApiUrl() {
        return eslApiUrl
    }

    @JsonProperty("eslApiUrl")
    void setEslApiUrl(String eslApiUrl) {
        this.eslApiUrl = eslApiUrl
    }

    @JsonProperty("uploadIdentityUrl")
    String getUploadIdentityUrl() {
        return uploadIdentityUrl
    }

    @JsonProperty("uploadIdentityUrl")
    void setUploadIdentityUrl(String uploadIdentityUrl) {
        this.uploadIdentityUrl = uploadIdentityUrl
    }

    @JsonProperty("uploadSignatureInsertionUrl")
    String getUploadSignatureInsertionUrl() {
        return uploadSignatureInsertionUrl
    }

    @JsonProperty("uploadSignatureInsertionUrl")
    void setUploadSignatureInsertionUrl(String uploadSignatureInsertionUrl) {
        this.uploadSignatureInsertionUrl = uploadSignatureInsertionUrl
    }

    @JsonProperty("uploadSignatureInsertionAAAPUrl")
    String getUploadSignatureInsertionAAAPUrl() {
        return uploadSignatureInsertionAAAPUrl
    }

    @JsonProperty("uploadSignatureInsertionAAAPUrl")
    void setUploadSignatureInsertionAAAPUrl(String uploadSignatureInsertionAAAPUrl) {
        this.uploadSignatureInsertionAAAPUrl = uploadSignatureInsertionAAAPUrl
    }

    @JsonProperty("uploadSignatureInsertionCDTUrl")
    String getUploadSignatureInsertionCDTUrl() {
        return uploadSignatureInsertionCDTUrl
    }

    @JsonProperty("uploadSignatureInsertionCDTUrl")
    void setUploadSignatureInsertionCDTUrl(String uploadSignatureInsertionCDTUrl) {
        this.uploadSignatureInsertionCDTUrl = uploadSignatureInsertionCDTUrl
    }

    @JsonProperty("viewRendererConfiguration")
    public Map<String, Map<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration;
    }

    @JsonProperty("viewRendererConfiguration")
    public void setViewRendererConfiguration(Map<String, Map<String, String>> viewRendererConfiguration) {
        ImmutableMap.Builder<String, Map<String, String>> builder = ImmutableMap.builder();
        for (Map.Entry<String, Map<String, String>> entry : viewRendererConfiguration.entrySet()) {
            builder.put(entry.getKey(), ImmutableMap.copyOf(entry.getValue()));
        }
        this.viewRendererConfiguration = builder.build();
    }

    @JsonProperty("dssPlatformHost")
    String getDssPlatformHost() {
        return dssPlatformHost
    }

    @JsonProperty("dssPlatformHost")
    void setDssPlatformHost(String dssPlatformHost) {
        this.dssPlatformHost = dssPlatformHost
    }

    @JsonProperty("dssPlatformPort")
    String getDssPlatformPort() {
        return dssPlatformPort
    }

    @JsonProperty("dssPlatformPort")
    void setDssPlatformPort(String dssPlatformPort) {
        this.dssPlatformPort = dssPlatformPort
    }

    @JsonProperty("catalogUrl")
    String getCatalogUrl() {
        return catalogUrl
    }

    @JsonProperty("catalogUrl")
    void setCatalogUrl(String catalogUrl) {
        this.catalogUrl = catalogUrl
    }

    @JsonProperty("fastTrackUrl")
    String getFastTrackUrl() {
        return fastTrackUrl
    }

    @JsonProperty("fastTrackUrl")
    void setFastTrackUrl(String fastTrackUrl) {
        this.fastTrackUrl = fastTrackUrl
    }
}
