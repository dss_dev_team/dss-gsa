package gov.dss.api.views

import io.dropwizard.views.View

/**
 *
 * A view for Upload Identities Complete
 *
 * Created by Carvel on 1/4/16 @ 7:03 AM.
 *
 */
class UploadIdentitiesCompleteView extends View {
    String[] messages
    String uploadIdentityUrl
    String eslApiUrl
    /**
     * Creates a new view.
     *
     * @param templateName the name of the template resource
     */
    protected UploadIdentitiesCompleteView(String templateName, String uploadIdentityUrl, String eslApiUrl, String[] messages) {
        super(templateName)
        this.messages = messages
        this.uploadIdentityUrl = uploadIdentityUrl
        this.eslApiUrl = eslApiUrl
    }
}
