package gov.dss.api.views

import io.dropwizard.views.View

/**
 *
 * A view for Upload Signatures
 *
 * Created by Sudhangi on 1/25/16 @ 11:00 AM.
 *
 */
class UploadSignatureInsertionCompleteView extends View {
    String[] messages
    String uploadSignatureInsertionUrl
    String eslApiUrl
    /**
     * Creates a new view.
     *
     * @param templateName the name of the template resource
     */
    protected UploadSignatureInsertionCompleteView(String templateName, String uploadSignatureInsertionUrl, String eslApiUrl, String[] messages) {
        super(templateName)
        this.messages = messages
        this.uploadSignatureInsertionUrl = uploadSignatureInsertionUrl
        this.eslApiUrl = eslApiUrl
    }
}
