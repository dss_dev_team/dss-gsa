package gov.dss.api

import com.hubspot.dropwizard.guice.GuiceBundle
import gov.dss.api.resources.HtmlResource
import gov.dss.api.resources.RestfulResource
import io.dropwizard.assets.AssetsBundle
import io.dropwizard.configuration.EnvironmentVariableSubstitutor
import io.dropwizard.configuration.SubstitutingSourceProvider
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.dropwizard.views.ViewBundle
import org.glassfish.jersey.media.multipart.MultiPartFeature

/**
 * The Dropwizard application for DSS
 *
 * Please see http://www.dropwizard.io for details
 *
 * Created by Carvel on 12/28/15 @ 1:26 PM.
 */
class Application extends io.dropwizard.Application<Configuration> {

    private GuiceBundle<Configuration> guiceBundle;
    /**
     * how to run an instance of the Application
     *
     * nohup java -jar dss-api-1.0-SNAPSHOT.jar  server /fully/qualified/path/to/dss.configuration.yaml
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        new Application().run(args)
    }

    @Override
    public String getName() {
        return "dss-integration";
    }

    /**
     * Initializes the application bootstrap.
     *
     * @param bootstrap the application bootstrap
     */
    @Override
    void initialize(Bootstrap<Configuration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        )

        bootstrap.addBundle(new AssetsBundle())
        bootstrap.addBundle(new ViewBundle<Configuration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(Configuration configuration) {
                return configuration.getViewRendererConfiguration()
            }
        });

        guiceBundle = GuiceBundle.<Configuration> newBuilder()
                .addModule(new Module())
                .setConfigClass(Configuration.class)
                .build();

        bootstrap.addBundle(guiceBundle);
    }

    /**
     * When the application runs, this is called after the {@link Bundle}s are run. Override it to add
     * providers, resources, etc. for your application.
     *
     * @param configuration the parsed {@link Configuration} object
     * @param environment the application's {@link Environment}
     * @throws Exception if something goes wrong
     */
    @Override
    void run(Configuration configuration, Environment environment) throws Exception {
        RestfulResource restfulResource = guiceBundle.getInjector().getInstance(RestfulResource.class)
        HtmlResource htmlResource = guiceBundle.getInjector().getInstance(HtmlResource.class)
        htmlResource.dssPlatformHost = configuration.dssPlatformHost
        htmlResource.dssPlatformPort = configuration.dssPlatformPort
        htmlResource.catalogUrl = String.format(configuration.catalogUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.fastTrackUrl = String.format(configuration.fastTrackUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.uploadIdentityUrl = String.format(configuration.uploadIdentityUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.uploadSignatureInsertionUrl = String.format(configuration.uploadSignatureInsertionUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.uploadSignatureInsertionAAAPUrl = String.format(configuration.uploadSignatureInsertionAAAPUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.uploadSignatureInsertionCDTUrl = String.format(configuration.uploadSignatureInsertionCDTUrl, htmlResource.dssPlatformHost, htmlResource.dssPlatformPort)
        htmlResource.eslApiUrl = configuration.eslApiUrl


        environment.jersey().register(new ExceptionHandler())
        environment.jersey().register(MultiPartFeature.class)
        environment.jersey().register(restfulResource)
        environment.jersey().register(htmlResource)
        environment.healthChecks().register("dss-integration", new TemplateHealthCheck());
    }
}
