package gov.dss.api.views

import io.dropwizard.views.View

/**
 * Author : Carvel 
 * Date   : 4/18/16
 */
class CatalogView extends View {
    String catalogUrl
    String fastTrackUrl
    String uploadIdentityUrl
    String uploadSignatureInsertionUrl

    /**
     * Creates a new view.
     *
     * @param templateName the name of the template resource
     */
    public CatalogView(String templateName, String catalogUrl, String fastTrackUrl, String uploadIdentityUrl, String uploadSignatureInsertionUrl) {
        super(templateName)
        this.catalogUrl = catalogUrl
        this.fastTrackUrl = fastTrackUrl
        this.uploadIdentityUrl = uploadIdentityUrl
        this.uploadSignatureInsertionUrl = uploadSignatureInsertionUrl
    }
}
