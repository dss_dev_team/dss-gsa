package gov.dss.api.resources

import com.google.common.base.Charsets
import com.google.inject.Inject
import gov.dss.api.views.CatalogView
import gov.dss.api.views.UploadIdentitiesCompleteView
import gov.dss.api.views.UploadSignatureInsertionCompleteView
import gov.dss.api.views.UploadSignatureInsertionView
import gov.dss.esl.sdk.DssEslClient
import gov.dss.esl.sdk.annontations.DefaultDssClientAnnotation
import io.dropwizard.views.View
import org.glassfish.jersey.media.multipart.FormDataBodyPart
import org.glassfish.jersey.media.multipart.FormDataParam

import javax.ws.rs.*
import javax.ws.rs.core.MediaType

/**
 *
 * A resource that serves html requests
 *
 * Created by Carvel on 1/2/16 @ 5:00 PM.
 *
 */
@Path("/views")
class HtmlResource {
    String dssPlatformHost
    String dssPlatformPort
    String eslApiUrl
    String catalogUrl
    String fastTrackUrl
    String uploadIdentityUrl
    String uploadSignatureInsertionUrl
    String uploadSignatureInsertionAAAPUrl
    String uploadSignatureInsertionCDTUrl
    DssEslClient dssEslClient

    @Inject
    public HtmlResource(@DefaultDssClientAnnotation DssEslClient dssEslClient) {
        this.dssEslClient = dssEslClient
    }

    @GET
    @Produces("text/html;charset=UTF-8")
    @Path("/uploadIdentities")
    public View uploadIdentities() {
        return new View("/views/ftl/upload/uploadIdentities.ftl", Charsets.ISO_8859_1) {

        }
    }

    @POST
    @Path("esl/accounts/uploadIdentities")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    UploadIdentitiesCompleteView uploadIdentities(@FormDataParam("identities") final InputStream inputStream) {
        def messages = dssEslClient.createIdentities(inputStream)
        return new UploadIdentitiesCompleteView("/views/ftl/upload/uploadIdentitiesComplete.ftl", uploadIdentityUrl, eslApiUrl, messages)
    }

    /**
     * @param fileInputStream
     * @param ownerEmail
     * @param tablesignersEmail
     * @param tablesignersFirstName
     * @param tablesignersLastName
     * @param tabletexts
     * @param tablesignType
     * @return
     */
    @POST
    @Path("esl/accounts/uploadSignatureInsertion")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    UploadSignatureInsertionCompleteView uploadSignatureInsertion(
            @FormDataParam("doc") final InputStream fileInputStream,
            @FormDataParam("documentName") final String documentName,
            @FormDataParam("ownerEmail") final String ownerEmail,
            @FormDataParam("newPackageName") final String newPackageName,
            @FormDataParam("tablesignersEmail") final List<FormDataBodyPart> tablesignersEmail,
            @FormDataParam("tablesignersFirstName") final List<FormDataBodyPart> tablesignersFirstName,
            @FormDataParam("tablesignersLastName") final List<FormDataBodyPart> tablesignersLastName,
            @FormDataParam("tabletexts") final List<FormDataBodyPart> tabletexts,
            @FormDataParam("tablesignType") final List<FormDataBodyPart> tablesignType,
            @FormDataParam("assignOrder") final String assignOrder,
            @FormDataParam("createSend") final String createSend) {
        List<String> listSignerEmails = new ArrayList<String>();
        List<String> listTexts = new ArrayList<String>();
        List<String> listSignTypes = new ArrayList<String>();
        List<String> listFirstName = new ArrayList<String>();
        List<String> listLastName = new ArrayList<String>();


        for (FormDataBodyPart signerEmailPart : tablesignersEmail) {
            listSignerEmails.add(signerEmailPart.getValueAs(String.class));
        }

        for (FormDataBodyPart textsPart : tabletexts) {
            listTexts.add(textsPart.getValueAs(String.class));
        }

        for (FormDataBodyPart signTypePart : tablesignType) {
            listSignTypes.add(signTypePart.getValueAs(String.class));
        }

        for (FormDataBodyPart firstNamePart : tablesignersFirstName) {
            if ((firstNamePart.getValueAs(String.class) == null) || (firstNamePart.getValueAs(String.class) == "")) {
                listFirstName.add("NullException");
            } else {
                listFirstName.add(firstNamePart.getValueAs(String.class));
            }
        }

        for (FormDataBodyPart lastNamePart : tablesignersLastName) {
            if ((lastNamePart.getValueAs(String.class) == null) || (lastNamePart.getValueAs(String.class) == "")) {
                listLastName.add("NullException");
            } else {
                listLastName.add(lastNamePart.getValueAs(String.class));
            }
        }

        def messages = dssEslClient.insertSignatureBlock(fileInputStream,
                documentName,
                                                        ownerEmail,
                newPackageName,
                                                        listSignerEmails,
                                                        listTexts,
                                                        listSignTypes,
                                                        listFirstName,
                                                        listLastName,
                                                        assignOrder,
                                                        createSend)
        return new UploadSignatureInsertionCompleteView("/views/ftl/signatureInsertion/uploadSignatureInsertionComplete.ftl", uploadSignatureInsertionUrl, eslApiUrl, messages)
    }

    /**
     * signature insertion view
     *
     * @return
     */
    @GET
    @Produces("text/html;charset=UTF-8")
    @Path("/selectApplication")
    public View selectApplication() {
        return new UploadSignatureInsertionView("/views/ftl/signatureInsertion/selectApplication.ftl", uploadSignatureInsertionAAAPUrl, uploadSignatureInsertionCDTUrl, uploadSignatureInsertionUrl) {

        }
    }

    @GET
    @Produces("text/html;charset=UTF-8")
    @Path("/uploadSignatureInsertionAAAP")
    public View uploadSignatureInsertionAAAP() {
        return new View("/views/ftl/signatureInsertion/uploadSignatureInsertionAAAP.ftl", Charsets.ISO_8859_1) {

        }
    }

    @GET
    @Produces("text/html;charset=UTF-8")
    @Path("/uploadSignatureInsertionCDT")
    public View uploadSignatureInsertionCDT() {
        return new View("/views/ftl/signatureInsertion/uploadSignatureInsertionCDT.ftl", Charsets.ISO_8859_1) {

        }
    }

    /**
     * fastTrack view
     *
     * @return
     */
    @GET
    @Path("/fasttrack")
    @Produces(MediaType.TEXT_HTML)
    View fastTrackIntegration() {
        return new View("/views/ftl/fasttrack/index.ftl", Charsets.UTF_8) {
        };
    }

    /**
     * catalog view
     *
     * @return
     */
    @GET
    @Path("/catalog")
    @Produces(MediaType.TEXT_HTML)
    View catalog() {
        return new CatalogView("/views/ftl/catalog/indexTabs.ftl", catalogUrl, fastTrackUrl, uploadIdentityUrl, uploadSignatureInsertionUrl);
    }

    @GET
    @Path("/home")
    @Produces(MediaType.TEXT_HTML)
    View home() {
        return new View("/views/ftl/home/indexTabs.ftl", Charsets.UTF_8) {
        };
    }
}
