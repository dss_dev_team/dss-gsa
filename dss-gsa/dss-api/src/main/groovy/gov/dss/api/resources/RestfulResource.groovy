package gov.dss.api.resources

import com.silanis.esl.sdk.DocumentPackage
import com.silanis.esl.sdk.DocumentPackageAttributes
import com.silanis.esl.sdk.PackageId
import gov.dss.esl.sdk.annontations.GenericCallbackHandlerAnnotation
import com.fasterxml.jackson.databind.ObjectMapper
import gov.dss.esl.sdk.annontations.IacpCallbackHandlerAnnotation
import gov.dss.esl.sdk.annontations.TspCallbackHandlerAnnotation
import gov.dss.esl.sdk.callbacks.CallbackHandler
import gov.dss.esl.sdk.callbacks.reta.RetaCallbackHandler
import org.apache.chemistry.opencmis.commons.impl.json.JSONObject
import org.apache.commons.lang3.StringUtils

import javax.inject.Inject
import javax.inject.Named
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType

/**
 * Version one of the DSS-API
 *
 * Created by Carvel on 12/28/15 @ 8:46 PM.
 *
 *
 */
@Path("/dss")
@Produces(MediaType.APPLICATION_JSON)
class RestfulResource {
    @Inject
    @IacpCallbackHandlerAnnotation
    CallbackHandler iacpCallbackHandler

    @Inject
    @TspCallbackHandlerAnnotation
    CallbackHandler tspCallbackHandler

    @Inject
    @GenericCallbackHandlerAnnotation
    CallbackHandler genericCallbackHandler

    RetaCallbackHandler retaCallbackHandler = new RetaCallbackHandler();

    @POST
    @Path("/v1/eslNotificationHandler")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    void executeCallbackHandler(
            @Context HttpServletRequest request, String sEvent) {
        HashMap<String,Object> mappedData =
                new ObjectMapper().readValue(sEvent, HashMap.class);
        def eventOccurred = mappedData.getAt("name");
        String packageIdString = mappedData.getAt("packageId");
        PackageId packageId = new PackageId(packageIdString)
        DocumentPackage documentPackage = genericCallbackHandler.dssEslClient.getPackage(packageId);
        DocumentPackageAttributes documentPackageAttributes = documentPackage.getAttributes();
        String packageName = documentPackage.getName();
        def orgName = documentPackageAttributes.getContents().get("orgName").toString();
        if ((StringUtils.isEmpty(orgName)) || (orgName == "null")) {
            if (documentPackage.getName().contains('ACP')) {
                orgName="IACP";
            }
        }

        switch (orgName) {
            /*TSP code has been commented out since it is now out of scope.
            case "TSP":
                println "tsp event: $eventOccurred"
                tspCallbackHandler.handleCallback(sEvent)
                break*/
            case "RETA":
                println "RETA event: $eventOccurred"
                retaCallbackHandler.retaPublishToQueue(eventOccurred, packageIdString, packageName, orgName, documentPackage);
                /*This genericCallbackHandler will not be used for RETA case.*/
                /*genericCallbackHandler.handleCallback(sEvent)*/
                break
            case "IACP":
                println "IACP event: $eventOccurred"
                iacpCallbackHandler.handleCallback(sEvent)
                break
            default:
                println "Default event occurred: $eventOccurred"
                break
        }
    }

    /**
     * Returns canned data so that Sudhangi can work on her User Interface for Signature
     * Insertion
     * @param request
     * @param sEvent
     */
    @POST
    @Path("v1/dss/dssUniversalConnector/CreatePackage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
     JSONObject dssUniversalConnector(@Context HttpServletRequest request, String data /* this is the json string*/ ) {

        HashMap<String,Object> mappedData =
        new ObjectMapper().readValue(data, HashMap.class);
        //This will be modified once the base code is modified such that it only uses one Api key for all the
        //applications.
        //Using tspApiKey and Url at this point since we don't know which is the final one.
        Map<String, Object> result = tspCallbackHandler.dssEslClient.dssUniversalConnector(mappedData);
        /*Convert Map to JSON string*/
        JSONObject jsonResult = new JSONObject();
        jsonResult.putAll( result );
        println "Rest call to dssUniversalConnector completed."
        return jsonResult;
    }
}
