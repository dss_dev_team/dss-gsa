#!/usr/bin/env bash
#
# This scripts creates a docker-machine if necessary and attaches
# the terminal to docker-machine
#
export DOCKER_HOST="tcp://0.0.0.0:2375"
echo "create docker machine for dss"
docker-machine create --driver virtualbox --virtualbox-memory=8096 --virtualbox-disk-size=90000 dss
echo "set up docker environment"
eval $(docker-machine env dss)
docker-machine ls
