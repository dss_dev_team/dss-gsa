#!/usr/bin/env bash

cd $DEV/dss-gsa/dss-gsa/dss-api/src/main/docker
cd ../../../../
mvn clean install
mkdir -p dss-api/src/main/docker/deploy
cp dss-api/target/dss-api-1.0-SNAPSHOT.jar  dss-api/src/main/docker/deploy/
cp dss-api/src/main/resources/dss.configuration.yaml  dss-api/src/main/docker/deploy/
cp dss-api/src/main/resources/application.properties  dss-api/src/main/docker/deploy/
cp dss-api/src/main/resources/google-drive-52541b4c3294.p12  dss-api/src/main/docker/deploy/
cd -
docker build -t dssdeveloperch/dss-api:latest .
docker save -o dss-api-docker-image.tar dss-api:latest
docker images
docker login --username=dssdeveloperch --email=dssdeveloper.ch@gmail.com
docker push dssdeveloperch/dss-api:latest

