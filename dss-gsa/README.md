#  DSS-API
# Prerequisites 
1. [Install java 1.8] (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. [Install maven] (https://maven.apache.org/install.html)
3. [Install git] (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 
4. Create firewall rules for ports two ports. Example: 8080, 8081. Port **_8080_** will need to
be accessible from the outside world because  will need to initiate callback handlers
via over http. Example http://some_server:8080/dss/esl/callback/handler
   
# Build dss-api web app
1. Clone DSS-GSA project from [Bitbucket] (https://bitbucket.org/dssgsadeveloper/dss-gsa) via terminal window: 
    * Example : git clone https://dssdeveloperch@bitbucket.org/dssgsadeveloper/dss-gsa.git
2. Checkout the appropriate branch from git
    * Example : git checkout feature/sprint9-development
3. cd dss-gsa
4. execute mvn clean install

# Run dss-api web app
1. Create a directory called gsa
2. cd gsa
3. Copy application.properties to new gsa directory
    * Example: cp dss-api/src/main/resources/application.properties gsa
4. Copy dss.configuration.yaml to new gsa directory
    * Example: cp dss-api/src/main/resources/dss.configuration.yaml gsa
5. Copy dss-api-1.0-SNAPSHOT.jar to new gsa directory
    * Example: cp dss-api/target/dss-api-1.0-SNAPSHOT.jar gsa
6. Run java -jar dss-api-1.0-SNAPSHOT.jar server dss.configuration.yaml
7. Open http://some_server:8080/dss/esl/fasttrack/integration in a browser

# Run dss-api web app in docker container for MAC users(Optional)
1. init docker-machine 'source src/main/docker/bin/docker-machine-init.sh'
2. go to dss-api directory and execute the following command 'mvn docker:build'
3. docker run -it -p 8080:8080 -p 8081:8081 dssdevelopch/dss-api:1.0-SNAPSHOT


# Deploy and run dss-api docker image on a remote machine(Optional)
1. run dss-api/src/main/docker/bin/deploy.sh
2. ssh to remote docker machine
3. docker login --username=dssdeveloperch --email=dssdeveloper.ch@gmail.com
4. docker pull dssdeveloperch/dss-api
5. docker run -d -p 8080:8080 -p 8081:8081 dssdeveloperch/dss-api
